﻿# Variables for common values
$resourceGroup = "WebResourceGroup"
$location = "westeurope"
$vmName = "dmpwebVM"
$vNetName = "vNet"

# Get-AzureRmVMSize

# Connect-AzureRmAccount

# Create user object
$cred = Get-Credential -Message "Enter a username and password for the virtual machine."

Connect-AzureRmAccount

# Create a resource group
New-AzureRmResourceGroup -Name $resourceGroup -Location $location

New-AzureRmVM -Name $vmName -Credential $cred -ResourceGroupName WebResourceGroup -Location $location -VirtualNetworkName $vNetName
 
Set-AzureRmVMExtension `
    -ResourceGroupName $resourceGroup `
    -ExtensionName IIS `
    -VMName $vmName `
    -Publisher Microsoft.Compute `
    -ExtensionType CustomScriptExtension `
    -TypeHandlerVersion 1.4 `
    -SettingString '{"commandToExecute":"powershell Add-WindowsFeature Web-Server,Web-Asp-Net45,NET-Framework-Features"}' `
    -Location $location
