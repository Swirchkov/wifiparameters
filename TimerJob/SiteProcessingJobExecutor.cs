﻿using System.Threading;
using CommonTypes.Entities;
using TimerJobRunner.Jobs;

namespace TimerJobRunner
{
    public class SiteProcessingJobExecutor : ISiteProcessingJobExecutor
    {
        public void RunSiteProcessingJob(TimerJob jobEntity)
        { 
            Thread t = new Thread(() =>
            {
                var job = new SiteProcessingJob();
                job.PrepareJob(jobEntity);
                job.ExecuteAsync(jobEntity);
            });

            t.Start();
        }
    }
}
