﻿
using HttpExecutor;
using CommonTypes.Entities;
using TimerJobRunner.JobResultHandlers;

namespace TimerJobRunner
{
    public interface ISiteProcessingJobExecutor
    {
        void RunSiteProcessingJob(TimerJob job);
    }
}
