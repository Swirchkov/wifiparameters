﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimerJobRunner.Jobs
{
    class TestJob : IJob
    {
        private static int executionNum = 0;

        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(() => { 
                executionNum++;
                Console.WriteLine("execution #{0}", executionNum);
                if (executionNum > 10)
                {
                    context.Scheduler.DeleteJob(context.Trigger.JobKey);
                }
            });
        }
    }
}
