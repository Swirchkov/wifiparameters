﻿using CommonTypes.Entities;
using HttpExecutor;
using HttpExecutor.JsonParsing;
using Quartz;
using System;
using System.Threading.Tasks;
using TimerJobRunner.DI;
using TimerJobRunner.JobResultHandlers;
using WifiParameters.DAL;
using Autofac;
using WifiParameters.DAL.RepositoryPrototypes;
using System.Threading;
using System.Diagnostics;

namespace TimerJobRunner.Jobs
{
    class SiteProcessingJob 
    {
        private IRepository<TimerJob> jobRepo;

        private IRepository<LoggedException> exceptionsRepo;

        private MonitoringDifferenceHandler handler = new MonitoringDifferenceHandler();

        private PageProcessor pageProcessor;

        private EventLog eventLog;

        public SiteProcessingJob()
        {
            jobRepo = DependencyResolver.Container.Resolve<IRepository<TimerJob>>();
            exceptionsRepo = DependencyResolver.Container.Resolve<IRepository<LoggedException>>();
            eventLog = new EventLog();
            eventLog.Source = "TimerService";
        }

        public void PrepareJob(TimerJob job)
        {
            this.pageProcessor = new PageProcessor(JsonConfigurationParser.ParseProcessingConfiguration(job.PrepareJobScenario));

            pageProcessor.ProcessPages();
            var newData = pageProcessor.RetrieveData;

            handler.SynchronizeDataState(newData, pageProcessor.DataWithoutExtensions, job);

            job.JobStatus = TimerJobStatus.Running;
            jobRepo.Update(job);
        }

        public async void ExecuteAsync(TimerJob job)
        {
            eventLog.WriteEntry("Async job execution " + job.TimerJobId);
            TimerJob currentJobState = null;
            try
            {
                currentJobState = jobRepo.Get(job.TimerJobId);

                if (currentJobState.JobStatus == TimerJobStatus.StopRequested)
                {
                    eventLog.WriteEntry("Stopping the job execution " + job.TimerJobId);
                    StopJob(currentJobState);
                    return;
                }

                ExecuteJob(currentJobState);
            }
            catch (Exception e)
            { 
                eventLog.WriteEntry(e.Message);

                exceptionsRepo.Add(new LoggedException()
                {
                    EntityId = job.TimerJobId,
                    EntityType = "TimerJob",
                    StackTrace = e.StackTrace,
                    ExceptionMessage = e.Message,
                    ExceptionType = e.GetType().Name,
                    OccuredUtc = DateTime.UtcNow
                });
                currentJobState.FailedRuns++;
                jobRepo.Update(currentJobState);
            }
            finally
            {
                if (currentJobState.JobStatus == TimerJobStatus.Running)
                {
                    await Task.Delay(TimeSpan.FromSeconds(currentJobState.ExecutionPeriod));
                    await Task.Run(() => { ExecuteAsync(currentJobState); });
                }
            }
        }

        private void ExecuteJob(TimerJob job)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            pageProcessor.CurrentConfiguration = JsonConfigurationParser.ParseProcessingConfiguration(job.TimerScenario);
            pageProcessor.ProcessPages();
            var newData = pageProcessor.RetrieveData;

            handler.SynchronizeDataState(newData, pageProcessor.DataWithoutExtensions, job);

            sw.Stop();

            job.AvgTime = ((job.AvgTime * job.SuccessRuns) + sw.Elapsed.Seconds) / (job.SuccessRuns + 1);
            job.SuccessRuns++;
            jobRepo.Update(job);
        }

        private void StopJob(TimerJob job)
        {
            job.JobStatus = TimerJobStatus.Stopped;
            jobRepo.Update(job);
            pageProcessor.Dispose();
        }
    }
}
