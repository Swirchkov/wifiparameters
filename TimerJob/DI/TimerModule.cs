﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace TimerJobRunner.DI
{
    public class TimerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SiteProcessingJobExecutor>().As<ISiteProcessingJobExecutor>();
            base.Load(builder);
        }
    }
}
