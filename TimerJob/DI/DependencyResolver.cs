﻿using Autofac;
using WifiParameters.DAL.DI;

namespace TimerJobRunner.DI
{
    internal static class DependencyResolver
    {
        private static IContainer container;

        public static IContainer Container
        {
            get
            {
                if (container == null)
                {
                    BuildContainer();
                }
                return container;
            }
        }

        private static void BuildContainer()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule(new DALModule());

            container = containerBuilder.Build();
        }
    }
}
