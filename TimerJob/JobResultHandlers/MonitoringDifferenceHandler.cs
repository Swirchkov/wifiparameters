﻿using CommonTypes;
using CommonTypes.Entities;
using HttpExecutor;
using NotificationsSender;
using System.Collections.Generic;
using System.Linq;
using TimerJobRunner.DI;
using WifiParameters.DAL.RepositoryPrototypes;
using Autofac;
using HttpExecutor.Models;
using HttpExecutor.JsonParsing;
using System;

namespace TimerJobRunner.JobResultHandlers
{
    public class MonitoringDifferenceHandler
    {
        private IJobDataRepository repo;

        private EmailSender sender = new EmailSender();

        public MonitoringDifferenceHandler()
        {
            repo = DependencyResolver.Container.Resolve<IJobDataRepository>();
        }

        public void SynchronizeDataState(RetrieveDataStorage dataStorage, List<Dictionary<string, string>> dataWithoutExtension, TimerJob job)
        {
            SiteProcessingConfiguration config = JsonConfigurationParser.ParseProcessingConfiguration(job.TimerScenario);

            ICollection<JobData> currentJobData = new List<JobData>(repo.FindBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(JobData.JobId), job.TimerJobId)
            }));

            int maxIndex = currentJobData.Count() > 0 ? currentJobData.Max(j => j.RowIndex) : 0;
            EmailSender sender = new EmailSender();

            for(var i=0; i < dataStorage.RetrievedData.Count; i++)
            {
                var rowDbRecords = GetJobDataRowByDataDictionary(currentJobData, dataWithoutExtension[i]);
                if (rowDbRecords == null || !rowDbRecords.Any())
                {
                    maxIndex++;
                    AddRecordsForDataRow(maxIndex, dataStorage.RetrievedData[i], job.TimerJobId);
                    AddUpdateLastFoundTime(config, job.TimerJobId, maxIndex);
                }
                else
                {
                    AddUpdateLastFoundTime(config, job.TimerJobId, rowDbRecords.First().RowIndex);
                }
                
            }
        }

        private void AddUpdateLastFoundTime(SiteProcessingConfiguration config, string jobId, int index)
        {
            JobData updateTimeRecord = repo.FindBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(JobData.JobId), jobId),
                new FilterCriteria(nameof(JobData.RowIndex), index.ToString()),
                new FilterCriteria(nameof(JobData.RetrieveName), config.ForEveryItem.UpdateTimeAlias)
            }).FirstOrDefault();

            if (updateTimeRecord != null)
            {
                updateTimeRecord.RetrievedData = DateTime.Now.ToString();
                repo.Update(updateTimeRecord);
            }
            else
            {
                repo.Add(new JobData()
                {
                    JobId = jobId,
                    RowIndex = index,
                    RetrievedData = DateTime.Now.ToString(),
                    RetrieveName = config.ForEveryItem.UpdateTimeAlias
                });
            }
        }

        private IEnumerable<JobData> GetJobDataRowByDataDictionary(ICollection<JobData> currentData, Dictionary<string, string> dictionary)
        {
            var dataGroupedByRows = currentData.GroupBy(j => j.RowIndex);

            return dataGroupedByRows.FirstOrDefault(groupedRows => 
            {
                foreach (string key in dictionary.Keys)
                {
                    if (groupedRows.Any(jd => jd.RetrieveName == key && jd.RetrievedData == dictionary[key]))
                    {
                        continue;
                    }
                    return false;
                }
                return true;
            });
        }

        private void AddRecordsForDataRow(int index, Dictionary<string, string> data, string jobId)
        {
            foreach (var key in data.Keys)
            {
                JobData jobData = new JobData()
                {
                    RetrievedData = data[key],
                    JobId = jobId,
                    RetrieveName = key,
                    RowIndex = index
                };
                repo.Add(jobData);
            }
            sender.SendNotification(MailMessageBuilder.BuildNewItemMessage(data));
        } 

        public void ProcessDataDifferences(DataDifferences differences, TimerJob job)
        {
            if (!differences.HasDifferences)
            {
                return;
            }

            IEnumerable<JobData> currentJobData = repo.FindBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(JobData.JobId), job.TimerJobId)
            });

            int maxIndex = currentJobData.Max(j => j.RowIndex);
            EmailSender sender = new EmailSender();

            foreach (var row in differences.AddedRows)
            {
                maxIndex++;
                foreach (var key in row.Keys)
                {
                    JobData jobData = new JobData()
                    {
                        RetrievedData = row[key],
                        JobId = job.TimerJobId,
                        RetrieveName = key,
                        RowIndex = maxIndex
                    };
                    repo.Add(jobData);
                }
                sender.SendNotification(MailMessageBuilder.BuildNewItemMessage(row));
            }

            foreach (var row in differences.RemovedRows)
            {
                foreach (var key in row.Keys)
                {
                    repo.DeleteBy(new List<FilterCriteria>()
                    {
                        new FilterCriteria(nameof(JobData.RetrieveName), key),
                        new FilterCriteria(nameof(JobData.RetrievedData), row[key])
                    });
                }
            }
        }

    }
}
