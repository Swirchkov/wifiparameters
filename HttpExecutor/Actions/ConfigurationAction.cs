﻿using OpenQA.Selenium;

namespace HttpExecutor.Actions
{
    public abstract class ConfigurationAction
    {
        public ActionType Type { get; private set; }

        public ConfigurationAction(ActionType type)
        {
            Type = type;
        }

        public abstract void Perform(IWebElement element, IWebDriver driver, PageProcessor processor = null);
    }
}
