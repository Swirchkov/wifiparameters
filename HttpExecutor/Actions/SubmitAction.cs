﻿
using OpenQA.Selenium;

namespace HttpExecutor.Actions
{
    public class SubmitAction : ConfigurationAction
    {
        public SubmitAction(ActionType type): base(type) { }

        public override void Perform(IWebElement element, IWebDriver driver, PageProcessor processor)
        {
            element.Submit();
        }
    }
}
