﻿
namespace HttpExecutor.Actions
{
    public enum ActionType
    {
        Unknown = 0,
        SetData = 1,
        RetrieveData = 2,
        Focus = 3,
        Click = 4,
        Submit = 5
    }
}
