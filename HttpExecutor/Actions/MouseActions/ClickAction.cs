﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Threading;

namespace HttpExecutor.Actions.MouseActions
{
    public class ClickAction : ConfigurationAction
    {
        public ClickAction(ActionType type) : base(type) { }

        public override void Perform(IWebElement element, IWebDriver driver, PageProcessor processor)
        {
            WebDriverWait waitDriver = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            waitDriver.Until(d => ExpectedConditions.ElementToBeClickable(element));
            waitDriver.Until(d => {
                if (element.Displayed && element.Enabled) {
                    return element;
                }
                return null;
            });
            waitDriver.Until(d =>
            {
                try
                {
                    element.Click();
                }
                catch (InvalidOperationException)
                {
                    // element can not be clicked
                    return null;
                }
                return element;
            });
        }
    }
}
