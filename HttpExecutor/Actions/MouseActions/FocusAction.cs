﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace HttpExecutor.Actions.MouseActions
{
    public class FocusAction : ConfigurationAction
    {
        public FocusAction(ActionType type) : base(type) { }

        public override void Perform(IWebElement element, IWebDriver driver, PageProcessor processor)
        {
            var action = new OpenQA.Selenium.Interactions.Actions(driver);
            action.MoveToElement(element);
            action.Perform();
        }
    }
}
