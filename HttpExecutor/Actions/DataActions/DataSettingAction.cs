﻿
using OpenQA.Selenium;

namespace HttpExecutor.Actions.DataActions
{
    public class DataSettingAction : ConfigurationAction
    {
        public string Data { get; private set; }

        public DataSettingAction(ActionType type, string data) : base(type)
        {
            Data = data;
        }

        public override void Perform(IWebElement element, IWebDriver driver, PageProcessor processor)
        {
            element.SendKeys(Data);
        }
    }
}
