﻿using OpenQA.Selenium;
using HttpExecutor.Models;
using System.Collections.Generic;
using System.Threading;
using HttpExecutor.Helpers;

namespace HttpExecutor.Actions.DataActions
{
    public class DataRetrievingAction : ConfigurationAction
    {

        public string RetrievePattern { get; private set; }

        public DataRetrievingAction(ActionType type, string retrievePattern) : base(type)
        {
            RetrievePattern = retrievePattern;
        }

        public override void Perform(IWebElement element, IWebDriver driver, PageProcessor processor)
        {
            ScrollHelper.ScrollToElement(driver, element);
            string elementText = element.Text;
            processor.RetrieveData.SetData(RetrievePattern, elementText);
        }
    }
}
