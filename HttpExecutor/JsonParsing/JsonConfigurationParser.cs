﻿using HttpExecutor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpExecutor.JsonParsing
{
    public static class JsonConfigurationParser
    {
        public static SiteProcessingConfiguration ParseProcessingConfiguration(string jsonText)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Converters.Add(new ActionsConverter());
            return JsonConvert.DeserializeObject<SiteProcessingConfiguration>(jsonText, settings);
        }
    }
}
