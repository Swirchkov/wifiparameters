﻿using System;
using HttpExecutor.Actions;
using HttpExecutor.Actions.DataActions;
using HttpExecutor.Actions.MouseActions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HttpExecutor.JsonParsing
{
    public class ActionsConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ConfigurationAction);
        }

        public ConfigurationAction ConvertToAction(JToken jAction)
        {
            ActionType actionType;
            if (!Enum.TryParse<ActionType>((string)jAction["type"], out actionType))
            {
                throw new ArgumentException("Invalid action in json data.");
            }

            if (actionType == ActionType.SetData)
            {
                return new DataSettingAction(actionType, (string)jAction["data"]);
            }

            if (actionType == ActionType.RetrieveData)
            {
                return new DataRetrievingAction(actionType, (string)jAction["retrievePattern"]);
            }

            if (actionType == ActionType.Click)
            {
                return new ClickAction(actionType);
            }

            if (actionType == ActionType.Focus)
            {
                return new FocusAction(actionType);
            }

            if (actionType == ActionType.Submit)
            {
                return new SubmitAction(actionType);
            }

            return null;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jAction = JObject.Load(reader);
            return ConvertToAction(jAction);
        }

        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
