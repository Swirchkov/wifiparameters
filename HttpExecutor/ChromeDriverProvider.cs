﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace HttpExecutor
{
    public static class ChromeDriverProvider
    {
        public static IWebDriver BuildChromeDriver()
        {
            ChromeOptions option = new ChromeOptions();
            // option.AddArgument("--headless");
            return new ChromeDriver(option);
        }
    }
}
