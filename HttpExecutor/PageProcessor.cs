﻿using HttpExecutor.Actions;
using HttpExecutor.Helpers;
using HttpExecutor.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HttpExecutor
{
    public class PageProcessor : IDisposable
    {
        private readonly int MaxPossibleTimeout = 60;

        private IWebDriver driver;

        private WebDriverWait waitDriver;

        public SiteProcessingConfiguration CurrentConfiguration { get; set; }

        private RetrieveDataStorage dataStorage = new RetrieveDataStorage();

        public RetrieveDataStorage RetrieveData
        {
            get
            {
                return dataStorage;
            }
        }

        public List<Dictionary<string, string>> DataWithoutExtensions; 

        public PageProcessor(SiteProcessingConfiguration configuration)
        {
            driver = ChromeDriverProvider.BuildChromeDriver();
            waitDriver = new WebDriverWait(driver, TimeSpan.FromSeconds(MaxPossibleTimeout));
            CurrentConfiguration = configuration;
        }

        ~PageProcessor()
        {
            Dispose();
        }

        public void Dispose()
        {
            driver.Dispose();
        }

        private void ResetStorage()
        {
            dataStorage = new RetrieveDataStorage();
            DataWithoutExtensions = new List<Dictionary<string, string>>();
        }

        public RetrieveDataStorage ProcessPages()
        {
            ResetStorage();
            var pages = CurrentConfiguration.Pages;

            foreach (var page in pages)
            {
                if (page.ShouldNavigateToPage)
                {
                    driver.Navigate().GoToUrl(page.LocationUrl);
                }

                if (page.ShouldReloadPage)
                {
                    driver.Navigate().Refresh();
                }

                WaitForPageLoad();

                foreach (var scenarioElement in page.PageScenario)
                {
                    ExecuteElementScenario(scenarioElement);
                }
            }

            RetrieveData.FlushData();
            ExtendFoundData();

            return RetrieveData;
        }

        private void ExtendFoundData()
        {
            DataWithoutExtensions = RetrieveData.CloneDataDictionary();

            if (CurrentConfiguration.ForEveryItem != null && CurrentConfiguration.ForEveryItem.AddFoundTime)
            {
                foreach (var row in RetrieveData.RetrievedData)
                {
                    if (row.Keys.Count > 0 && !row.Keys.Contains(CurrentConfiguration.ForEveryItem.FoundTimeAlias))
                    row[CurrentConfiguration.ForEveryItem.FoundTimeAlias] = DateTime.Now.ToString();
                }
            }
        }

        private void ExecuteElementScenario(ScenarioElement scenarioElement, Dictionary<string, string> envParams = null)
        {
            IEnumerable<IWebElement> webElements = WaitForElements(By.CssSelector(scenarioElement.ElementSelector), waitDriver);

            if (!string.IsNullOrWhiteSpace(scenarioElement.ElementValueEquals))
            {
                string criteria = scenarioElement.ElementValueEquals;

                if (criteria.StartsWith("@"))
                {
                    criteria = envParams[criteria];
                }

                webElements = FilterElementsByText(criteria, webElements);
            }

            foreach (ConfigurationAction action in scenarioElement.Actions)
            {
                foreach (var element in webElements.Where(e => e.Displayed))
                {
                    action.Perform(element, driver, this);
                }
            }
        }

        private IEnumerable<IWebElement> FilterElementsByText(string criteria, IEnumerable<IWebElement> elements)
        {
            return elements.Where(e =>
            {
                ScrollHelper.ScrollToElement(driver, e);
                return e.Text.Equals(criteria);
            });
        }

        private IEnumerable<IWebElement> WaitForElements(By selector, WebDriverWait waitDriver)
        {
            try
            {
                waitDriver.Until(d =>
                {
                    try
                    {
                        var elements = d.FindElements(selector);
                        return elements.FirstOrDefault(e => e.Displayed != false);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                });
                return driver.FindElements(selector);
            }
            catch (Exception)
            {
                return new List<IWebElement>();
            }
        }

        private void WaitForPageLoad()
        {
            waitDriver.Until(d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
        }

    }
}
