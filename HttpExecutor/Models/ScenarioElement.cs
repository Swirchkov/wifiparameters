﻿
using HttpExecutor.Actions;
using System.Collections.Generic;

namespace HttpExecutor.Models
{
    public class ScenarioElement
    {
        public string ElementSelector { get; set; }

        public string ElementValueEquals { get; set; }

        public IEnumerable<ConfigurationAction> Actions { get; set; } 
    }
}
