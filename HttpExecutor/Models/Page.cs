﻿
using System.Collections.Generic;

namespace HttpExecutor.Models
{
    public class Page
    {
        public string LocationUrl { get; set; }

        public IEnumerable<ScenarioElement> PageScenario { get; set; }

        public bool ShouldNavigateToPage { get; set; }

        public bool ShouldReloadPage { get; set; }

    }
}
