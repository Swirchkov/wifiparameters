﻿
namespace HttpExecutor.Models
{
    public class DataFoundHandler
    {
        public bool AddFoundTime { get; set; }
        public string FoundTimeAlias { get; set; }

        public bool AddUpdateTime { get; set; }
        public string UpdateTimeAlias { get; set; }
    }
}
