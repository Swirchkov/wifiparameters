﻿using System.Collections.Generic;

namespace HttpExecutor.Models
{
    public class SiteProcessingConfiguration
    {
        public IEnumerable<Page> Pages { get; set; }

        public DataFoundHandler ForEveryItem { get; set; }
    }
}
