﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpExecutor
{
    public class DataDifferences
    {
        public DataDifferences()
        {
            AddedRows = new List<Dictionary<string, string>>();
            RemovedRows = new List<Dictionary<string, string>>();
        }

        public List<Dictionary<string, string>> AddedRows { get; private set; }

        public List<Dictionary<string, string>> RemovedRows { get; private set; }

        public bool HasDifferences
        {
            get
            {
                return AddedRows.Count > 0 || RemovedRows.Count > 0;
            }
        }
    }
}
