﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HttpExecutor
{
    public class RetrieveDataStorage
    {
        public List<Dictionary<string, string>> RetrievedData { get; private set; } = new List<Dictionary<string, string>>();

        private Dictionary<string, string> retrievingRow = null;

        public RetrieveDataStorage() { }

        /// <summary>
        /// Saves value by specified key
        /// </summary>
        /// <param name="key"> Data key </param>
        /// <param name="value"> Saved value </param>
        /// <returns> True if saving value causes creating new data row, and false if its not.</returns>
        public bool SetData(string key, string value)
        {
            foreach (var row in RetrievedData)
            {
                if (!row.ContainsKey(key))
                {
                    row.Add(key, value);
                    return false;
                }

                if (row[key].Equals(value, StringComparison.InvariantCulture))
                {
                    return false;
                }
            }

            if (retrievingRow != null && !retrievingRow.Keys.Contains(key))
            {
                retrievingRow.Add(key, value);
                return false;
            }

            AddRow();
            retrievingRow.Add(key, value);
            return true;
        }

        public void FlushData()
        {
            if (retrievingRow != null && retrievingRow.Keys.Count > 0)
            {
                AddRow();
            }
        }

        private void AddRow()
        {
            if (retrievingRow != null)
            {
                RetrievedData.Add(retrievingRow);
            }

            retrievingRow = new Dictionary<string, string>();
        }

        public DataDifferences GetDifferencesWith(RetrieveDataStorage other)
        {
            DataDifferences differences = new DataDifferences();

            foreach (var row in RetrievedData)
            {
                if (!other.RetrievedData.Any(r => CompareDictionaries(r, row)))
                {
                    differences.RemovedRows.Add(row);
                }
            }

            foreach (var row in other.RetrievedData)
            {
                if (!RetrievedData.Any(r => CompareDictionaries(r, row)))
                {
                    differences.AddedRows.Add(row);
                }
            }

            return differences;
        }

        public List<Dictionary<string, string>> CloneDataDictionary()
        {
            var result = new List<Dictionary<string, string>>();

            foreach (var dataRow in RetrievedData)
            {
                var rowCopy = new Dictionary<string, string>();
                foreach (var key in dataRow.Keys)
                {
                    rowCopy.Add(key, dataRow[key]);
                }
                result.Add(rowCopy);
            }

            return result;
        }

        private bool CompareDictionaries(Dictionary<string, string> d1, Dictionary<string, string> d2)
        {
            if (d1.Keys.Count != d2.Keys.Count)
            {
                return false;
            }

            foreach (var key1 in d1.Keys)
            {
                if (!(d2.ContainsKey(key1) && d1[key1].Equals(d2[key1], StringComparison.InvariantCulture))) 
                {
                    return false;
                }
            }
            return true;
        }

    }
}
