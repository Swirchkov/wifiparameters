﻿
using CommonTypes.Messages;
using System;
using TimerJobRunner;
using TimerService.DI;
using Autofac;
using System.Diagnostics;
using System.Threading;

namespace TimerService.MessageHandlers
{
    public class StartJobMessageHandler : IMessageHandler
    {
        private EventLog eventLog;

        public StartJobMessageHandler()
        {
            eventLog = new EventLog();
            eventLog.Source = "TimerService";
        }

        public void ProcessMessage(BaseMessage message)
        {
            StartJobMessage startJobMessage = message as StartJobMessage;

            if (startJobMessage == null)
            {
                throw new ArgumentException("Incorrect argument type", nameof(message));
            }

            ISiteProcessingJobExecutor siteJobExecutor = TimerServiceContainer.Resolver.Resolve<ISiteProcessingJobExecutor>();

            eventLog.WriteEntry(String.Format("Starting job execution. Job id - {0} , job name - {1}",
                startJobMessage.TimerJob.TimerJobId, startJobMessage.TimerJob.JobName));

            siteJobExecutor.RunSiteProcessingJob(startJobMessage.TimerJob);
        }
    }
}
