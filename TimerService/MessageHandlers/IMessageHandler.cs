﻿
using CommonTypes.Messages;

namespace TimerService.MessageHandlers
{
    public interface IMessageHandler
    {
        void ProcessMessage(BaseMessage message);
    }
}
