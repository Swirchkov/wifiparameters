﻿using CommonTypes.Entities;
using CommonTypes.Messages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Messaging;
using TimerService.DI;
using TimerService.MessageHandlers;
using TimerService.XML;
using Autofac;
using WifiParameters.DAL;

namespace TimerService
{
    public class MessageListenerThread
    {
        private EventLog eventLog;

        private readonly XmlConverter xmlConverter = new XmlConverter();

        private readonly IRepository<LoggedException> exceptionsRepo;

        private Dictionary<Type, IMessageHandler> messageHandlers =
            new Dictionary<Type, IMessageHandler>()
            {
                { typeof(StartJobMessage), new StartJobMessageHandler() }
            };

        public MessageListenerThread()
        {
            eventLog = new EventLog();
            eventLog.Source = "TimerService";
            exceptionsRepo = TimerServiceContainer.Resolver.Resolve<IRepository<LoggedException>>();

        }

        public void Start()
        {
            try
            {
                string queuePath = ConfigurationManager.AppSettings["MessageQueueKey"];
                MessageQueue queue = new MessageQueue(queuePath);

                while (true)
                {
                    BaseMessage message = ReadMessageBody(queue.Receive());
                    eventLog.WriteEntry("Message received in windows service");

                    messageHandlers[message.GetType()].ProcessMessage(message);
                }
            }
            catch (Exception e)
            {
                eventLog.WriteEntry(String.Format("Exception happpened type - '{0}', message - '{1}', stack trace - '{2}'", e.GetType().FullName, e.Message, e.StackTrace));
                exceptionsRepo.Add(new LoggedException()
                {
                    ExceptionMessage = e.Message,
                    ExceptionType = e.GetType().FullName,
                    OccuredUtc = DateTime.Now,
                    StackTrace = e.StackTrace
                });

            }
        }

        private BaseMessage ReadMessageBody(Message message)
        {
            message.Formatter = new ActiveXMessageFormatter();
            return xmlConverter.ConvertXMLToMessage(message.BodyStream);
        }
    }
}
