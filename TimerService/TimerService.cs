﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using TimerJobRunner;
using Timer = System.Timers.Timer;

namespace TimerService
{
    public partial class TimerService : ServiceBase
    {

        private EventLog eventLog;

        private bool enabled;

        private MessageListenerThread messageListener = new MessageListenerThread();

        private Thread listenerThread;

        public TimerService()
        {
            InitializeComponent();
            eventLog = new EventLog();
            eventLog.Source = "TimerService";
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                eventLog.WriteEntry("Starting service");
                enabled = true;

                listenerThread = new Thread(new ThreadStart(messageListener.Start));
                listenerThread.Start();
            }
            catch (Exception e)
            {
                EventLog.WriteEntry(e.Message);
            }
        }

        protected override void OnStop()
        {
            eventLog.WriteEntry("Stopping service");
            listenerThread.Abort();
        }
    }
}
