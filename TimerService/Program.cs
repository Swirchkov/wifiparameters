﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using CommonTypes;

namespace TimerService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            string dbConnection = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            DbManagementData.ConnectionString = dbConnection;
            var eventLog = new EventLog();
            eventLog.Source = "TimerService";

            eventLog.WriteEntry(dbConnection);

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new TimerService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
