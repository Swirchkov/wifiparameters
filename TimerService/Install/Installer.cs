﻿using System.ComponentModel;
using System.Diagnostics;
using System.ServiceProcess;

namespace TimerService.Install
{
    [RunInstaller(true)]
    public partial class Installer : System.Configuration.Install.Installer
    {
        ServiceInstaller serviceInstaller;
        ServiceProcessInstaller processInstaller;

        private readonly string eventLogSource = "TimerService";

        private readonly string eventLogName = "TimerService";

        public Installer()
        {
            InitializeComponent();

            serviceInstaller = new ServiceInstaller();
            processInstaller = new ServiceProcessInstaller();

            processInstaller.Account = ServiceAccount.LocalService;
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.ServiceName = "TimerService";
            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}
