﻿using Autofac;
using TimerJobRunner.DI;
using WifiParameters.DAL.DI;

namespace TimerService.DI
{
    static class TimerServiceContainer
    {
        private static IContainer resolver = null;

        public static IContainer Resolver
        {
            get
            {
                if (resolver == null)
                {
                    resolver = BuildResolver();
                }
                return resolver;
            }
        }

        public static IContainer BuildResolver()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new DALModule());
            builder.RegisterModule(new TimerModule());

            return builder.Build();
        }
    }
}
