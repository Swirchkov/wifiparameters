﻿using CommonTypes.Messages;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace TimerService.XML
{
    public class XmlConverter
    {
        public BaseMessage ConvertXMLToMessage(Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(StartJobMessage));
            var xmlReader = XmlReader.Create(stream);

            if (serializer.CanDeserialize(xmlReader))
            {
                return (StartJobMessage)serializer.Deserialize(xmlReader);
            }

            return null;
        } 
    }
}
