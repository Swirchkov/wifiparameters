﻿
namespace WifiParameters.API.Models.User
{
    public class LoginResponseModel
    {
        public string UserId { get; set; }

        public string AccountId { get; set; }
    }
}