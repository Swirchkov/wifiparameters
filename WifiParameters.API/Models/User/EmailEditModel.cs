﻿
namespace WifiParameters.API.Models.User
{
    public class EmailEditModel
    {
        public string UserId { get; set; }

        public string Email { get; set; }
    }
}