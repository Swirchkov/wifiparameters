﻿
namespace WifiParameters.API.Models.User
{
    public class PasswordChangeModel
    {
        public string UserId { get; set; }

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}