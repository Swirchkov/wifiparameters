﻿using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WifiParameters.API.Models
{

    public class DataExtensionViewModel : JobDataExtension
    {
        public new DataCondition DataCondition { get; set; }

        public string DataCriteriaName { get; set; }

        public string CriteriaValue { get; set; }

        public JobDataExtension ToEntity()
        {
            return new JobDataExtension()
            {
                TimerJobId = TimerJobId,
                DataExtensionId = DataExtensionId,
                DataColumnName = DataColumnName,
                Value = Value,
                DataCondition = new DataCondition()
                {
                    FieldName = DataCriteriaName,
                    CriteriaValue = CriteriaValue
                }
            };
        }
    }
}