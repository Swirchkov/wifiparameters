﻿using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WifiParameters.API.Models
{
    public class TimerJobViewModel
    {
        public TimerJobViewModel() { }

        public TimerJobViewModel(TimerJob job)
        {
            TimerJobId = job.TimerJobId;
            JobName = job.JobName;
            ExecutionPeriod = job.ExecutionPeriod;
            JobStatus = job.JobStatus.ToString();
        }

        public string TimerJobId { get; set; }

        public string JobName { get; set; }

        public int ExecutionPeriod { get; set; }

        public string JobStatus { get; set; }
    }
}