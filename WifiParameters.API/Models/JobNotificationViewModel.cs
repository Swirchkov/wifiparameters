﻿using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WifiParameters.API.Models
{
    public class JobNotificationViewModel
    {

        public string NotificationId { get; set; }

        public string JobId { get; set; }

        public string NotificationName { get; set; }

        public string HtmlTemplate { get; set; }

        public string FieldName { get; set; }

        public string FieldValue { get; set; }

        public JobNotification ToJobNotification()
        {
            return new JobNotification()
            {
                NotificationId = NotificationId,
                NotificationName = NotificationName,
                JobId = JobId,
                HtmlTemplate = HtmlTemplate,
                CriteriaCondition = new DataCondition()
                {
                    FieldName = FieldName,
                    CriteriaValue = FieldValue
                }
            };
        }

        public static JobNotificationViewModel CreateViewModel(JobNotification entity)
        {
            return new JobNotificationViewModel()
            {
                NotificationId = entity.NotificationId,
                JobId = entity.JobId,
                NotificationName = entity.NotificationName,
                HtmlTemplate = entity.HtmlTemplate,
                FieldName = entity.CriteriaCondition.FieldName,
                FieldValue = entity.CriteriaCondition.CriteriaValue
            };
        }
    }
}