﻿using CommonTypes;
using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WifiParameters.API.Models;
using WifiParameters.BLL.ServicePrototypes;

namespace WifiParameters.API.Controllers
{
    public class TimerJobController : CUDController<TimerJob, IJobService>
    {

        [HttpGet]
        public IEnumerable<TimerJobViewModel> GetJobsByHost(string hostId)
        {
            return service.FilterBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(TimerJob.HostId), hostId)
            }).Select(job => new TimerJobViewModel(job));
        }

        [HttpPost]
        [Route("api/timerjob/runjob")]
        public IHttpActionResult RunJob([FromBody]string jobId)
        {
            try
            {
                service.RunJob(jobId);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpPost]
        [Route("api/timerjob/stopjob")]
        public IHttpActionResult StopJob([FromBody]string jobId)
        {
            try
            {
                service.StopJob(jobId);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        } 

    }
}
