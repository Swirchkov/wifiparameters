﻿using CommonTypes;
using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Web.Http;
using WifiParameters.API.Models;
using WifiParameters.BLL;
using System.Linq;
using DependencyResolver = System.Web.Mvc.DependencyResolver;

namespace WifiParameters.API.Controllers
{
    public class JobNotificationController : ApiController
    {
        private IService<JobNotification> service;

        public JobNotificationController()
        {
            service = DependencyResolver.Current.GetService(typeof(IService<JobNotification>)) 
                as IService<JobNotification>;
        }

        [HttpGet]
        public IEnumerable<JobNotificationViewModel> GetJobNotifications(string jobId)
        {
            return service.FilterBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(JobNotification.JobId), jobId)
            }).Select(j => JobNotificationViewModel.CreateViewModel(j));
        }

        [HttpPost]
        public IHttpActionResult CreateNotification(JobNotificationViewModel notificationModel)
        {
            var notificationEntity = notificationModel.ToJobNotification();
            try
            {
                service.Add(notificationEntity);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(notificationEntity.JobId);
        }

        [HttpDelete]
        public IHttpActionResult DeleteNotification(string entityId)
        {
            try
            {
                service.Delete(entityId);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }
    }
}