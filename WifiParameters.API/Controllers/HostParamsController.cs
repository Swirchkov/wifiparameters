﻿using CommonTypes;
using CommonTypes.Entities;
using System.Web.Http;
using System.Collections.Generic;
using WifiParameters.BLL;

namespace WifiParameters.API.Controllers
{
    public class HostParamsController : CUDController<HostParameters, IService<HostParameters>>
    {

        [HttpGet]
        public IEnumerable<HostParameters> GetParamsByHost(string hostId)
        {
            return service.FilterBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(HostParameters.HostId), hostId)
            });
        }

    }
}
