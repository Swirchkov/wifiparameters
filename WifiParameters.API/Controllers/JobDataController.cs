﻿using CommonTypes;
using CommonTypes.Entities;
using CommonTypes.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using WifiParameters.BLL;
using WifiParameters.BLL.ServicePrototypes;

namespace WifiParameters.API.Controllers
{
    public class JobDataController : ApiController
    {
        private IJobDataService jobDataService;

        public JobDataController()
        {
            jobDataService = DependencyResolver.Current.GetService<IJobDataService>();
        }

        [System.Web.Http.HttpGet]
        public TableViewModel GetDataByJobId(string jobId)
        {
            return jobDataService.BuildJobDataViewTable(jobId);
        }
    }
}
