﻿using CommonTypes;
using CommonTypes.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using WifiParameters.API.Models.User;
using WifiParameters.BLL;
using WifiParameters.BLL.Exceptions;
using WifiParameters.BLL.ServicePrototypes;

namespace WifiParameters.API.Controllers
{
    public class UserController : ApiController
    {
        private IUserService userService;

        private IService<Account> accountService;
       
        public UserController()
        {
            userService = DependencyResolver.Current.GetService<IUserService>();
            accountService = DependencyResolver.Current.GetService<IService<Account>>();
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/user/login")]
        public LoginResponseModel Login(LoginViewModel model)
        {
            User user = userService.FilterBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(CommonTypes.Entities.User.Email), model.Email),
                new FilterCriteria(nameof(CommonTypes.Entities.User.Password), model.Password)
            }).AsEnumerable().FirstOrDefault();

            if (user != null)
            {
                return new LoginResponseModel()
                {
                    UserId = user.UserId,
                    AccountId = user.AccountId
                };
            }

            return null;
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/user/updateemail")]
        public IHttpActionResult UpdateUserEmail(EmailEditModel model)
        {
            try
            {
                userService.UpdateEmail(model.UserId, model.Email);
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("api/user/updatepassword")]
        public IHttpActionResult UpdateUserPassword(PasswordChangeModel model)
        {
            try
            {
                userService.UpdatePassword(model.UserId, model.OldPassword, model.NewPassword);
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }
    }
}
