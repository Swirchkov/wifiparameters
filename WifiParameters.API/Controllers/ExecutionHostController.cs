﻿using CommonTypes.Entities;
using System.Web.Http;
using WifiParameters.BLL.ServicePrototypes;
using System.Linq;

namespace WifiParameters.API.Controllers
{
    public class ExecutionHostController : CUDController<JobExecutionHost, IExecutionHostService> 
    {

        [HttpGet]
        [ActionName("availableHosts")]
        public IHttpActionResult GetAvailableExecutionHosts()
        {
            var hosts = service.GetAllAvailableHosts().ToList();
            return Json(hosts);
        }

    }
}
