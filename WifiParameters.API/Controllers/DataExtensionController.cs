﻿using CommonTypes;
using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Web.Http;
using WifiParameters.API.Models;
using WifiParameters.BLL;

namespace WifiParameters.API.Controllers
{
    public class DataExtensionController : CUDController<JobDataExtension, IService<JobDataExtension>>
    {

        [HttpGet]
        public IEnumerable<JobDataExtension> GetDataExtensionsByJob(string jobId)
        {
            return service.FilterBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(JobDataExtension.TimerJobId), jobId)
            });
        }

    }
}
