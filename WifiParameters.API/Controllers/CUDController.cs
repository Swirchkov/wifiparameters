﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using WifiParameters.BLL;

namespace WifiParameters.API.Controllers
{
    public class CUDController<T, TServiceType> : ApiController 
        where T : class 
        where TServiceType : IService<T> 
    {
        protected TServiceType service;

        public CUDController()
        {
            service = DependencyResolver.Current.GetService<TServiceType>();
        }

        [System.Web.Http.HttpPost]
        public virtual IHttpActionResult AddEntity(T entity)
        {
            try
            {
                service.Add(entity);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(entity);
        }

        [System.Web.Http.HttpPut]
        public IHttpActionResult UpdateEntity(T entity)
        {
            try
            {
                service.Add(entity);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        [System.Web.Http.HttpDelete]
        public IHttpActionResult DeleteEntity(string entityId)
        {
            try
            {
                service.Delete(entityId);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }
    }
}
