﻿using CommonTypes;
using System.Configuration;

namespace WifiParameters.API.App_Start
{
    public class DBInitializer
    {
        public static void Initialize()
        {
            DbManagementData.ConnectionString = 
                ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;

        }
    }
}