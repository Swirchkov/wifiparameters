﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using WifiParameters.BLL.DI;

namespace WifiParameters.API.App_Start
{
    public class DIConfig
    {
        public static void InitializeApplication()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule<BLLModule>();
            containerBuilder.RegisterControllers(typeof(WebApiApplication).Assembly);

            DependencyResolver.SetResolver(new AutofacDependencyResolver(containerBuilder.Build()));
        }
    }
}