﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NotificationsSender
{
    public static class MailMessageBuilder
    {
        public static MailMessage BuildNewItemMessage(Dictionary<string, string> item)
        {
            var message = new MailMessage("data.platform2018@gmail.com", "swirchkov@gmail.com");
            message.Subject = "New item found";

            message.IsBodyHtml = true;

            foreach (var pair in item)
            {
                message.Body += string.Format("<div> Found monitoring data by {0} - {1}", pair.Key, pair.Value);
            }

            return message;
        }
    }
}
