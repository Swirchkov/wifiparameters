﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HttpExecutor.Tests
{
    [TestClass]
    public class DataStorageTests
    {
        [TestMethod]
        public void VerifyDataSetting()
        {
            // arrange
            RetrieveDataStorage storage = new RetrieveDataStorage();

            // act 
            storage.SetData("key1", "value1");
            storage.SetData("key2", "value2");
            storage.SetData("key1", "value3");
            storage.SetData("key2", "value4");
            storage.SetData("key1", "value5");

            // assert 
            Assert.AreEqual(2, storage.RetrievedData.Count);

            Assert.AreEqual("value1", getValueFromStorage(0, "key1", storage));
            Assert.AreEqual("value2", getValueFromStorage(0, "key2", storage));
            Assert.AreEqual("value4", getValueFromStorage(1, "key2", storage));
        }

        [TestMethod]
        public void VerifyDataFlushing()
        {
            // arrange
            RetrieveDataStorage storage = new RetrieveDataStorage();

            // act 
            storage.SetData("key1", "value1");
            storage.SetData("key2", "value2");
            storage.SetData("key1", "value3");
            storage.SetData("key2", "value4");
            storage.SetData("key1", "value5");
            storage.FlushData();

            // assert 
            Assert.AreEqual(3, storage.RetrievedData.Count);

            Assert.AreEqual("value1", getValueFromStorage(0, "key1", storage));
            Assert.AreEqual("value2", getValueFromStorage(0, "key2", storage));
            Assert.AreEqual("value4", getValueFromStorage(1, "key2", storage));
            Assert.AreEqual("value5", getValueFromStorage(2, "key1", storage));
        }

        [TestMethod]
        public void VerifyDataDifferences()
        {
            #region arrange

            RetrieveDataStorage storage1 = new RetrieveDataStorage();

            storage1.SetData("key1", "value1");
            storage1.SetData("key2", "value2");
            storage1.SetData("key1", "value3");
            storage1.SetData("key2", "value4");
            storage1.SetData("key1", "value5");
            storage1.SetData("key2", "value6");
            storage1.FlushData();

            RetrieveDataStorage storage2 = new RetrieveDataStorage();

            storage2.SetData("key1", "value1");
            storage2.SetData("key2", "value2");
            storage2.SetData("key1", "value7");
            storage2.SetData("key2", "value8");
            storage2.SetData("key1", "value9");
            storage2.SetData("key2", "value10");
            storage2.FlushData();

            #endregion

            // act 
            var difference11 = storage1.GetDifferencesWith(storage1);
            var difference12 = storage1.GetDifferencesWith(storage2);
            var difference22 = storage2.GetDifferencesWith(storage2);

            // assert
            Assert.AreEqual(0, difference11.AddedRows.Count);
            Assert.AreEqual(0, difference11.RemovedRows.Count);
            Assert.AreEqual(0, difference22.AddedRows.Count);
            Assert.AreEqual(0, difference22.RemovedRows.Count);

            // check 12 difference
            Assert.AreEqual(2, difference12.AddedRows.Count);
            Assert.AreEqual(2, difference12.RemovedRows.Count);
        }

        private string getValueFromStorage(int rowIndex, string key, RetrieveDataStorage storage)
        {
            return storage.RetrievedData[rowIndex][key];
        }
    }
}
