﻿using CommonTypes;
using CommonTypes.Entities;
using HttpExecutor;
using HttpExecutor.JsonParsing;
using HttpExecutor.Models;
using System;
using System.IO;
using System.Reflection;
using WifiParameters.DAL;
using WifiParameters.DAL.Repositories;
using TimerJobRunner;
using System.Threading;
using WifiParameters.BLL.ServicePrototypes;
using WifiParameters.BLL.Services;
using Autofac;
using System.Messaging;
using CommonTypes.Messages;
using System.Xml.Serialization;
using System.Xml;

namespace TestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // local
            DbManagementData.ConnectionString = @"Data Source=DESKTOP-18IK4LP\SQLSERVERMAIN;Database=DMP;Integrated Security=True";

            // aws db
            // DbManagementData.ConnectionString = @"Data Source=dmp-db.cxgskhdpxa7c.us-east-2.rds.amazonaws.com,1433;Database=DMP;User Id=Admin;Password=Napoleon1";

            // TestMsMq();

            //IJobService jobService = AutofacTestContainer.Resolver.Resolve<IJobService>();
            //jobService.RunJob("00b6563d-ff7f-4dff-ad1b-41ccab0179e4");

            // RunTestJob();
            // TestConfigurationProcessing();
            RepositoryTester.RunTests();
            // TestTimerJob();
            // ServiceTester.RunTests();
        }

        static void TestMsMq()
        {
            var queues =  MessageQueue.GetPrivateQueuesByMachine("desktop-18ik4lp");
            MessageQueue queue = new MessageQueue(@"FormatName:DIRECT=OS:desktop-18ik4lp\private$\timerqueue");
            queue.Send(new StartJobMessage()
            {
                TimerJob = new TimerJob()
                {
                    HostId = Guid.NewGuid().ToString(),
                    TimerJobId = Guid.NewGuid().ToString(),
                    AvgTime = 10,
                    ExecutionPeriod = 10,
                    FailedRuns = 0,
                    JobName = "test job",
                    SuccessRuns = 110,
                    JobStatus = TimerJobStatus.Stopped,
                    PrepareJobScenario = "<json>",
                    TimerScenario = "<json>"
                }
            });

            var message = queue.Receive();
            message.Formatter = new ActiveXMessageFormatter();
        }

        static void TestConfigurationProcessing()
        {
            string executionDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            SiteProcessingConfiguration siteProcess =
                JsonConfigurationParser.ParseProcessingConfiguration(File.ReadAllText(Path.Combine(executionDir, "cloud.processing.zones.json")));

            Console.WriteLine("Configuration has been parsed");

            var processor = new PageProcessor(siteProcess);
            var foundData =  processor.ProcessPages();
            processor.Dispose();
        }

        static void TestTimerJob()
        {
            // string zonesJobId = "6f0f9f78-8541-41ed-b591-d7c411b631d6";
            // string zonesRegularJobId = "1060749d-8182-4e7e-b718-b4b9608b8fb4";
            string clientsJobId = "e3b9606e-99e6-41a7-a825-7fd4d080fc28";

            string jobId = clientsJobId;
            IJobService jobService = AutofacTestContainer.Resolver.Resolve<IJobService>();
            Console.WriteLine("test timer job execution");
            jobService.RunJob(jobId);

            Thread.Sleep(TimeSpan.FromMinutes(1));

            Console.WriteLine("Stop attempted");
            jobService.StopJob(jobId);

            Thread.Sleep(TimeSpan.FromSeconds(30));
        }

        static void RunTestJob()
        {
            var executor = new TestJobExecutor();
            executor.Test();
            Thread.Sleep(TimeSpan.FromMinutes(9));
        }
    }
}
