﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WifiParameters.BLL.ServicePrototypes;
using Autofac;
using CommonTypes.Entities;
using CommonTypes;

namespace TestConsoleApp
{
    static class ServiceTester
    {
        public static void RunTests()
        {
            // TestExecutionHostService();
            TestJobDataService();
        }

        public static void TestExecutionHostService()
        {
            IExecutionHostService hostService = AutofacTestContainer.Resolver.Resolve<IExecutionHostService>();

            var jobHost = new JobExecutionHost()
            {
                HostName = "google",
                HostUrl = "www.google.com"
            };

            var hostParams = new List<HostParameters>()
            {
                new HostParameters()
                {
                    ParameterName = "login",
                    ParameterValue = "login1"
                },
                new HostParameters()
                {
                    ParameterName = "password",
                    ParameterValue = "pass123"
                }
            };

            hostService.CreateExecutionHost(jobHost, hostParams);
        }

        public static void TestJobDataService()
        {
            string jobId = "6f0f9f78-8541-41ed-b591-d7c411b631d6";

            IJobDataService jobDataService = AutofacTestContainer.Resolver.Resolve<IJobDataService>();

            var jobDataTable = jobDataService.BuildJobDataViewTable(jobId);

            Console.WriteLine(jobDataTable);
        }
    }
}
