﻿using CommonTypes;
using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using WifiParameters.DAL;
using WifiParameters.DAL.Repositories;
using Autofac;

namespace TestConsoleApp
{
    class RepositoryTester
    {
        public static void RunTests()
        {
            // TestsAccountsRepository();
            TestHostsRepositories();
            TestParametersRepository();
            TestJobsRepository();
            TestJobsDataRepository();
            TestDataExtensionsRepository();
            TestNotificationSubscriptionRepository();
            TestExceptionsRepository();

            CleanUpData();
        }

        static void TestsAccountsRepository()
        {
            IRepository<Account> accountRepo = AutofacTestContainer.Resolver.Resolve<IRepository<Account>>();

            accountRepo.Add(new Account()
            {
                AccountId = "11",
                AccountName = "US_ACC"
            });

        }

        static void TestHostsRepositories()
        {
            IRepository<JobExecutionHost> hostRepo = AutofacTestContainer.Resolver.Resolve<IRepository<JobExecutionHost>>();

            var testHost = new JobExecutionHost()
            {
                HostId = "11",
                HostName = "host",
                HostUrl = "www.google.com",
                AccountId = "11"
            };

            hostRepo.Add(testHost);

            var dbHost = hostRepo.Get("11");
            Console.WriteLine(" obj name - {0}, db name - {1}", testHost.HostName, dbHost.HostName);

            var hosts = hostRepo.FindBy(new List<FilterCriteria>()
            {
                new FilterCriteria()
                {
                    FieldName = nameof(JobExecutionHost.HostName),
                    FieldValue = "host"
                }
            });

            Console.WriteLine(hosts.Count());
        }

        static void TestParametersRepository()
        {
            var testParams = new HostParameters()
            {
                ParameterId = "111",
                ParameterName = "par_name",
                ParameterValue = "par_value",
                HostId = "11"
            };

            IRepository<HostParameters> parameterRepo = AutofacTestContainer.Resolver.Resolve<IRepository<HostParameters>>();

            parameterRepo.Add(testParams);

            var dbParams = parameterRepo.Get("111");
            Console.WriteLine(" obj name - {0}, db name - {1}", testParams.ParameterName, dbParams.ParameterName);
        }

        static void TestJobsRepository()
        {
            var testJob = new TimerJob()
            {
                TimerJobId = "111",
                JobName = "job_name",
                PrepareJobScenario = "scenario",
                TimerScenario = "time scenario",
                ExecutionPeriod = 1,
                HostId = "11"
            };

            IRepository<TimerJob> jobRepo = AutofacTestContainer.Resolver.Resolve<IRepository<TimerJob>>();

            jobRepo.Add(testJob);

            var dbParams = jobRepo.Get("111");
            Console.WriteLine(" obj name - {0}, db name - {1}", testJob.JobName, dbParams.JobName);
        }

        static void TestJobsDataRepository()
        {
            var testJobData = new JobData()
            {
                JobDataId = "1111",
                JobId = "111",
                RetrievedData = "page data",
                RetrieveName = "retrieve alias"
            };

            IRepository<JobData> jobDataRepo = AutofacTestContainer.Resolver.Resolve<IRepository<JobData>>();

            jobDataRepo.Add(testJobData);

            var dbParams = jobDataRepo.Get("1111");
            Console.WriteLine(" obj name - {0}, db name - {1}", testJobData.RetrieveName, dbParams.RetrieveName);
        }

        static void TestDataExtensionsRepository()
        {

            var testDataExtension = new JobDataExtension()
            {
                DataExtensionId = "11111",
                TimerJobId = "111",
                DataColumnName = "page data",
                Value = "retrieve alias",
                DataCondition = new DataCondition()
                {
                    FieldName = "MacAddress",
                    CriteriaValue = "23:A3:45:6F:DA:C3"
                }
            };

            IRepository<JobDataExtension> dataExtensionsRepo = AutofacTestContainer.Resolver.Resolve<IRepository<JobDataExtension>>();

            dataExtensionsRepo.Add(testDataExtension);

            var dbParams = dataExtensionsRepo.Get("11111");
            Console.WriteLine(" extension name - {0}, placed value - {1}", testDataExtension.DataColumnName, dbParams.Value);
        }

        static void TestNotificationSubscriptionRepository()
        {

            var testSubscription = new JobNotification()
            {
                NotificationId = "2222",
                JobId = "111",
                HtmlTemplate = "<<html>>",
                CriteriaCondition = new DataCondition()
                {
                    FieldName = "MacAddress",
                    CriteriaValue = "23:A3:45:6F:DA:C3"
                }
            };

            IRepository<JobNotification> dataSubscriptionsRepo = AutofacTestContainer.Resolver.Resolve<IRepository<JobNotification>>();

            dataSubscriptionsRepo.Add(testSubscription);

            var dbEntity = dataSubscriptionsRepo.Get("2222");
            Console.WriteLine("Notification id - {0}, html template - {1}", dbEntity.NotificationId, dbEntity.HtmlTemplate);
        }

        static void TestExceptionsRepository()
        {
            IRepository<LoggedException> exceptionsRepo = AutofacTestContainer.Resolver.Resolve<IRepository<LoggedException>>();

            var testException = new LoggedException()
            {
                ExceptionId = "222",
                EntityId = Guid.NewGuid().ToString(),
                EntityType = "TimerJob",
                ExceptionMessage = "Not found message",
                StackTrace = "at line 154",
                ExceptionType = "NotFoundException",
                OccuredUtc = DateTime.UtcNow
            };

            exceptionsRepo.Add(testException);

            var dbException = exceptionsRepo.Get("222");
            Console.WriteLine(" obj type - {0}, db type - {1}", testException.ExceptionType, dbException.ExceptionType);

            var exceptions = exceptionsRepo.FindBy(new List<FilterCriteria>()
            {
                new FilterCriteria()
                {
                    FieldName = nameof(LoggedException.EntityType),
                    FieldValue = "TimerJob"
                }
            });

            Console.WriteLine(exceptions.Count());
        }

        static void CleanUpData()
        {
            IRepository<JobData> jobDataRepo = AutofacTestContainer.Resolver.Resolve<IRepository<JobData>>();
            IRepository<TimerJob> timerJobRepo = AutofacTestContainer.Resolver.Resolve<IRepository<TimerJob>>();
            IRepository<HostParameters> paramsRepo = AutofacTestContainer.Resolver.Resolve<IRepository<HostParameters>>();
            IRepository<JobExecutionHost> hostRepo = AutofacTestContainer.Resolver.Resolve<IRepository<JobExecutionHost>>();

            IRepository<LoggedException> exceptionsRepo = AutofacTestContainer.Resolver.Resolve<IRepository<LoggedException>>();

            jobDataRepo.Delete("1111");
            timerJobRepo.Delete("111");
            paramsRepo.Delete("111");
            hostRepo.Delete("11");
            exceptionsRepo.Delete("222");
        }
    }
}
