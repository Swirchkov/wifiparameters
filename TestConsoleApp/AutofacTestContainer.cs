﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using WifiParameters.BLL.DI;
using WifiParameters.DAL.DI;

namespace TestConsoleApp
{
    static class AutofacTestContainer
    {
        private static IContainer resolver;

        public static IContainer Resolver
        {
            get
            {
                if (resolver == null)
                {
                    BuildResolver();
                }
                return resolver;
            }
        }

        private static void BuildResolver()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<BLLModule>();
            builder.RegisterModule<DALModule>();

            resolver = builder.Build();
        }
    }
}
