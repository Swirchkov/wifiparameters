﻿using Autofac;
using TimerJobRunner.DI;
using WifiParameters.DAL.DI;

namespace WifiParameters.BLL.DI
{
    public static class BLLDependencyContainer
    {
        private static IContainer container;

        private static IContainer BuildContainer()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterModule<DALModule>();
            containerBuilder.RegisterModule<TimerModule>();

            return containerBuilder.Build();
        }

        public static IContainer Resolver
        {
            get
            {
                container = container ?? BuildContainer();
                return container;
            }
        }
    }
}
