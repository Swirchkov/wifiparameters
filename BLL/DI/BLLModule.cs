﻿using Autofac;
using Autofac.Core;
using TimerJobRunner.DI;
using WifiParameters.BLL.ServicePrototypes;
using WifiParameters.BLL.Services;
using WifiParameters.DAL.DI;

namespace WifiParameters.BLL.DI
{
    public class BLLModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DALModule>();
            builder.RegisterModule<TimerModule>();

            builder.RegisterGeneric(typeof(Service<>)).As(typeof(IService<>));

            builder.RegisterType<ExecutionHostService>().As<IExecutionHostService>();
            builder.RegisterType<JobService>().As<IJobService>();
            builder.RegisterType<JobDataService>().As<IJobDataService>();
            builder.RegisterType<UserService>().As<IUserService>();

            base.Load(builder);
        }
    }
}
