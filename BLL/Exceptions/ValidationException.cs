﻿using System;

namespace WifiParameters.BLL.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException() : base("Entity validation exception") { }

        public ValidationException(string message) : base(message) { }

    }
}
