﻿
using System.Collections.Generic;
using CommonTypes;

namespace WifiParameters.BLL
{
    public interface IService<T> where T: class
    {
        IEnumerable<T> FilterBy(IEnumerable<FilterCriteria> filters);

        T GetById(string id);

        void Add(T item);

        void Update(T item);

        void Delete(string id);
    }
}
