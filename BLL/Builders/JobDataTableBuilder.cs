﻿using CommonTypes.Entities;
using CommonTypes.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WifiParameters.BLL.Builders
{
    static class JobDataTableBuilder
    {
        public static TableViewModel BuildJobDataTable(IEnumerable<JobData> jobRelatedData, 
            IEnumerable<JobDataExtension> jobExtensionData)
        {
            TableViewModel tableView = new TableViewModel();

            if (jobRelatedData.Count() == 0)
            {
                return tableView;
            }

            tableView.ColumnHeaders = GetColumnHeaders(jobRelatedData, jobExtensionData);

            tableView.Rows = BuildEmptyMatrix(tableView.ColumnHeaders.Count, 
                jobRelatedData.Select(j => j.RowIndex).Distinct().Count());

            var maxMatrixIndex = 0;

            var dbToMatrixIndexMap = new Dictionary<int, int>();

            foreach (var jobData in jobRelatedData)
            {
                if (!dbToMatrixIndexMap.ContainsKey(jobData.RowIndex))
                {
                    dbToMatrixIndexMap.Add(jobData.RowIndex, maxMatrixIndex);
                    maxMatrixIndex++;
                }
                int columnIndex = tableView.ColumnHeaders.FindIndex(name => name.Equals(jobData.RetrieveName, StringComparison.InvariantCulture));
                tableView.Rows[dbToMatrixIndexMap[jobData.RowIndex]][columnIndex] = jobData.RetrievedData;

                IEnumerable<JobDataExtension> associatedDataExtensions = jobExtensionData.Where(ext => ext.DataCondition.FieldName == jobData.RetrieveName 
                && ext.DataCondition.CriteriaValue == jobData.RetrievedData);

                foreach (var extension in associatedDataExtensions)
                {
                    columnIndex = tableView.ColumnHeaders.FindIndex(name => name.Equals(extension.DataColumnName, StringComparison.InvariantCulture));

                    tableView.Rows[dbToMatrixIndexMap[jobData.RowIndex]][columnIndex] = extension.Value;
                }
            }

            return tableView;
        }

        private static List<string> GetColumnHeaders(IEnumerable<JobData> jobRelatedData,
            IEnumerable<JobDataExtension> jobExtensionData)
        {
            var columnHeaders = new List<string>();

            columnHeaders = jobRelatedData.Select(data => data.RetrieveName).Distinct().ToList();

            columnHeaders.AddRange(jobExtensionData.Where(ext => jobRelatedData.Any(data =>
                data.RetrieveName == ext.DataCondition.FieldName && data.RetrievedData == ext.DataCondition.CriteriaValue))
                .Select(ext => ext.DataColumnName));

            return columnHeaders;
        }

        private static List<List<string>> BuildEmptyMatrix(int columnNum, int rowsNum)
        {
            var rows = new List<List<string>>();

            for (int i = 0; i < rowsNum; i++)
            {
                var emptyRow = new List<string>();
                for (int j = 0; j < columnNum; j++)
                {
                    emptyRow.Add(string.Empty);
                }
                rows.Add(emptyRow);
            }

            return rows;
        }

    }
}
