﻿using CommonTypes.Entities;
using System.Collections.Generic;

namespace WifiParameters.BLL.ServicePrototypes
{
    public interface IExecutionHostService : IService<JobExecutionHost>
    {
        IEnumerable<JobExecutionHost> GetAllAvailableHosts();

        void CreateExecutionHost(JobExecutionHost host, IEnumerable<HostParameters> hostParameters);
    }
}
