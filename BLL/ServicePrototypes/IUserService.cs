﻿using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiParameters.BLL.ServicePrototypes
{
    public interface IUserService : IService<User>
    {
        void UpdateEmail(string userId, string email);

        void UpdatePassword(string userId, string oldPassword, string newPassword);
    }
}
