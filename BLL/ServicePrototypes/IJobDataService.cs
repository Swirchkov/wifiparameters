﻿using CommonTypes.Entities;
using CommonTypes.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiParameters.BLL.ServicePrototypes
{
    public interface IJobDataService : IService<JobData>
    {
        TableViewModel BuildJobDataViewTable(string jobId);
    }
}
