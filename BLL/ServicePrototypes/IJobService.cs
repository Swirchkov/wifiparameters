﻿using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiParameters.BLL.ServicePrototypes
{
    public interface IJobService : IService<TimerJob>
    {
        void RunJob(string jobId);
        void StopJob(string jobId);
    }
}
