﻿using System;
using System.Collections.Generic;
using CommonTypes;
using CommonTypes.Entities;
using WifiParameters.DAL;
using WifiParameters.BLL.DI;
using Autofac;

namespace WifiParameters.BLL.Services
{
    public class Service<T> : IService<T> where T : class
    {
        protected IRepository<T> repo;

        private IRepository<LoggedException> exceptionRepo;

        public Service(IRepository<T> repo)
        {
            this.repo = repo;
            exceptionRepo = BLLDependencyContainer.Resolver.Resolve<IRepository<LoggedException>>();
        }

        public T GetById(string id)
        {
            return repo.Get(id);
        }

        public void Add(T item)
        {
            repo.Add(item);
        }

        public void Delete(string id)
        {
            repo.Delete(id);
        }

        public IEnumerable<T> FilterBy(IEnumerable<FilterCriteria> filters)
        {
            return repo.FindBy(filters);
        }

        public void LogException(Exception e, string entityId)
        {
            exceptionRepo.Add(new LoggedException()
            {
                EntityId = entityId,
                StackTrace = e.StackTrace,
                EntityType = typeof(T).Name,
                ExceptionMessage = e.Message,
                ExceptionType = e.GetType().Name,
                OccuredUtc = DateTime.UtcNow
            });
        }

        public void Update(T item)
        {
            repo.Update(item);
        }
    }
}
