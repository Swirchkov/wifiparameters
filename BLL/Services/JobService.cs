﻿using CommonTypes.Entities;
using HttpExecutor.Models;
using HttpExecutor.JsonParsing;
using WifiParameters.BLL.ServicePrototypes;
using HttpExecutor;
using TimerJobRunner;
using WifiParameters.DAL.RepositoryPrototypes;
using NotificationsSender;
using System;
using System.Collections.Generic;
using System.Messaging;
using System.Configuration;
using CommonTypes.Messages;
using WifiParameters.DAL;

namespace WifiParameters.BLL.Services
{
    public class JobService : Service<TimerJob>, IJobService
    {
        private IJobDataRepository jobDataRepo;

        private IRepository<TimerJob> timerJobRepo;

        private ISiteProcessingJobExecutor timerJobExecutor;

        public JobService(IRepository<TimerJob> jobRepo,
            IJobDataRepository jobDataRepo,
            ISiteProcessingJobExecutor jobExecutor) : base(jobRepo) {
            this.jobDataRepo = jobDataRepo;
            timerJobExecutor = jobExecutor;
            timerJobRepo = jobRepo;
        }

        public void RunJob(string jobId)
        {
            try
            {
                TimerJob job = repo.Get(jobId);

                jobDataRepo.ClearAllJobData(jobId);

                string queuePath = ConfigurationManager.AppSettings["MessageQueueKey"];
                MessageQueue queue = new MessageQueue(queuePath);
                queue.Send(new StartJobMessage() { TimerJob = job });
            }
            catch (Exception e)
            {
                LogException(e, jobId);
                throw;
            }
        }

        public void StopJob(string jobId)
        {
            TimerJob job = repo.Get(jobId);

            job.JobStatus = TimerJobStatus.StopRequested;
            timerJobRepo.Update(job);
        }
    }
}
