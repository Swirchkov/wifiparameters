﻿using CommonTypes.Entities;
using System.Collections.Generic;
using WifiParameters.DAL.RepositoryPrototypes;
using WifiParameters.BLL.ServicePrototypes;
using WifiParameters.DAL;
using System;

namespace WifiParameters.BLL.Services
{
    public class ExecutionHostService : Service<JobExecutionHost>, IService<JobExecutionHost>, IExecutionHostService 
    {
        private IExecutionHostRepository hostRepository;

        private IRepository<HostParameters> paramsRepository;

        public ExecutionHostService(IExecutionHostRepository repo, IRepository<HostParameters> paramsRepo) : base(repo)
        {
            hostRepository = repo;
            paramsRepository = paramsRepo;
        }

        public void CreateExecutionHost(JobExecutionHost host, IEnumerable<HostParameters> hostParameters)
        {
            host.HostId = Guid.NewGuid().ToString();
            hostRepository.Add(host);

            foreach (var param in hostParameters)
            {
                param.HostId = host.HostId;
                param.ParameterId = Guid.NewGuid().ToString();
                paramsRepository.Add(param);
            }

        }

        public IEnumerable<JobExecutionHost> GetAllAvailableHosts()
        {
            return hostRepository.GetAllHosts();
        }
    }
}
