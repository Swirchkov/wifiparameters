﻿using CommonTypes.Entities;
using WifiParameters.BLL.Exceptions;
using WifiParameters.BLL.ServicePrototypes;
using WifiParameters.DAL;

namespace WifiParameters.BLL.Services
{
    internal class UserService : Service<User>, IUserService 
    {

        public UserService(IRepository<User> userRepo) : base(userRepo) { }

        public void UpdateEmail(string userId, string email)
        {
            User dbUser = repo.Get(userId);
            dbUser.Email = email;
            repo.Update(dbUser);
        }

        public void UpdatePassword(string userId, string oldPassword, string newPassword)
        {
            User dbUser = repo.Get(userId);

            if (dbUser.Password != oldPassword)
            {
                throw new ValidationException("Old password doesn't match current");
            }

            dbUser.Password = newPassword;
            repo.Update(dbUser);
        }
    }
}
