﻿using CommonTypes;
using CommonTypes.Entities;
using CommonTypes.ViewModels;
using HttpExecutor.JsonParsing;
using HttpExecutor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WifiParameters.BLL.Builders;
using WifiParameters.BLL.ServicePrototypes;
using WifiParameters.DAL;

namespace WifiParameters.BLL.Services
{
    public class JobDataService : Service<JobData>, IJobDataService
    {
        private IRepository<JobDataExtension> extensionsRepo;

        private IRepository<TimerJob> jobRepo;

        public JobDataService(IRepository<JobData> repo,
            IRepository<JobDataExtension> extensionsRepo, 
            IRepository<TimerJob> jobRepo): base(repo) {
            this.extensionsRepo = extensionsRepo;
            this.jobRepo = jobRepo;
        }

        public TableViewModel BuildJobDataViewTable(string jobId)
        {
            IEnumerable<JobData> jobRelatedData = repo.FindBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(JobData.JobId), jobId)
            });

            IEnumerable<JobDataExtension> jobExtensionData = extensionsRepo.FindBy(new List<FilterCriteria>()
            {
                new FilterCriteria(nameof(JobDataExtension.TimerJobId), jobId)
            });

            TimerJob jobEntity = jobRepo.Get(jobId);
            SiteProcessingConfiguration config = JsonConfigurationParser.ParseProcessingConfiguration(jobEntity.TimerScenario);

            // jobRelatedData = SortDataCollection(jobRelatedData, config);

            var jobDataTable = JobDataTableBuilder.BuildJobDataTable(jobRelatedData, jobExtensionData);
            jobDataTable = SortTableData(jobDataTable, config);
            return jobDataTable;
        }

        private TableViewModel SortTableData(TableViewModel model, SiteProcessingConfiguration config)
        {
            int sortingColumnNum = model.ColumnHeaders.FindIndex(col => col == config.ForEveryItem.UpdateTimeAlias);
            model.Rows = model.Rows.OrderBy(l => l[sortingColumnNum]).ToList();
            return model;
        }

        private IEnumerable<JobData> SortDataCollection(IEnumerable<JobData> data, SiteProcessingConfiguration config)
        {
            if (config.ForEveryItem.UpdateTimeAlias != null)
            {
                return data.OrderBy(jd => 
                {
                    if (jd.RetrieveName != config.ForEveryItem.UpdateTimeAlias)
                    {
                        return -1;
                    }
                    return DateTime.Parse(jd.RetrievedData).ToFileTime();
                });
            }

            return data;
        }
    }
}
