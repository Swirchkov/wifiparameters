﻿
using System.Collections.Generic;

namespace CommonTypes
{
    public class FilterCriteria
    {
        public FilterCriteria() { }

        public FilterCriteria(string filterName, string value)
        {
            FieldName = filterName;
            FieldValue = value;
        }

        public string FieldName { get; set; }

        public string FieldValue { get; set; }

        public static string BuildWhereClause(IEnumerable<FilterCriteria> filters)
        {
            string whereClause = string.Empty;

            foreach (var filter in filters)
            {
                whereClause += string.Format("{0} = '{1}' AND ", filter.FieldName, filter.FieldValue);
            }

            // remove last and operator in where clause
            whereClause = whereClause.Substring(0, whereClause.Length - 4);

            return whereClause;
        }
    }
}
