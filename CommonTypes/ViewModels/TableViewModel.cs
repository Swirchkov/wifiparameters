﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonTypes.ViewModels
{
    public class TableViewModel
    {
        public List<string> ColumnHeaders { get; set; }

        public List<List<string>> Rows { get; set; }
    }
}
