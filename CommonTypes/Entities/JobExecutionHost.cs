﻿using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class JobExecutionHost
    {
        public JobExecutionHost() { }

        public JobExecutionHost(SqlDataReader reader)
        {
            HostId = (string)reader["HostId"];
            HostName = (string)reader["HostName"];
            HostUrl = (string)reader["HostUrl"];
        }

        public string HostId { get; set; }

        public string HostName { get; set; }

        public string HostUrl { get; set; }

        public string AccountId { get; set; }
    }
}
