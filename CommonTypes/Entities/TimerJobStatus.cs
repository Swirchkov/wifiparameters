﻿
namespace CommonTypes.Entities
{
    public enum TimerJobStatus
    {
        Stopped = 0,
        Running = 1,
        StopRequested = 2
    }
}
