﻿using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class Account
    {
        public Account() { }

        public Account(SqlDataReader reader)
        {
            AccountId = (string)reader["AccountId"];
            AccountName = (string)reader["AccountName"];
        }

        public string AccountId { get; set; }

        public string AccountName { get; set; }

    }
}
