﻿
using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class JobData
    {
        public JobData() { }

        public JobData(SqlDataReader reader)
        {
            JobDataId = (string)reader["JobDataId"];
            JobId = (string)reader["JobId"];
            RetrieveName = (string)reader["RetrieveName"];
            RetrievedData = (string)reader["RetrievedData"];
            RowIndex = (int)reader["RowIndex"];
        }

        public string JobDataId { get; set; }

        public string JobId { get; set; }

        public string RetrieveName { get; set; }

        public string RetrievedData { get; set; }

        public int RowIndex { get; set; }
    }
}
