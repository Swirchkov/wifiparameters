﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonTypes.Entities
{
    public class User
    {
        public User() { }

        public User(SqlDataReader reader)
        {
            UserId = (string)reader["UserId"];
            Email = (string)reader["Email"];
            Password = (string)reader["Password"];
            AccountId = (string)reader["AccountId"];
        }

        public string UserId { get; set; }

        public string AccountId { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}
