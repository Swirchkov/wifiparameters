﻿
using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class HostParameters
    {
        public HostParameters() { }

        public HostParameters(SqlDataReader reader)
        {
            ParameterId = (string)reader["ParameterId"];
            ParameterName = (string)reader["ParameterName"];
            ParameterValue = (string)reader["ParameterValue"];
            HostId = (string)reader["HostId"];
        }

        public string ParameterId { get; set; }

        public string ParameterName { get; set; }

        public string ParameterValue { get; set; }

        public string HostId { get; set; }
    }
}
