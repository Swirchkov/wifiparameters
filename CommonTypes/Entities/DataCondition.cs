﻿using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class DataCondition
    {
        public DataCondition() { }

        public DataCondition(SqlDataReader reader)
        {
            DataConditionId = (string)reader["DataConditionId"];
            FieldName = (string)reader["FieldName"];
            CriteriaValue = (string)reader["CriteriaValue"];
        }

        public string DataConditionId { get; set; }

        public string FieldName { get; set; }

        public string CriteriaValue { get; set; }
    }
}
