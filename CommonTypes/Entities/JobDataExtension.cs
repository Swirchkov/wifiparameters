﻿
using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class JobDataExtension
    {
        public JobDataExtension() { }

        public JobDataExtension(SqlDataReader reader)
        {
            DataExtensionId = (string)reader["DataExtensionId"];
            TimerJobId = (string)reader["TimerJobId"];
            DataColumnName = (string)reader["DataColumnName"];
            Value = (string)reader["Value"];
            DataCondition = new DataCondition(reader);
        }

        public string DataExtensionId { get; set; }

        public string TimerJobId { get; set; }

        public string DataColumnName { get; set; }

        public string Value { get; set; }

        public DataCondition DataCondition { get; set; }
    }
}
