﻿
using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class TimerJob
    {
        public TimerJob() { }

        public TimerJob(SqlDataReader reader)
        {
            TimerJobId = (string)reader["TimerJobId"];
            JobName = (string)reader["JobName"];
            PrepareJobScenario = (string)reader["PrepareJobScenario"];
            TimerScenario = (string)reader["TimerScenario"];
            ExecutionPeriod = (int)reader["ExecutionPeriod"];
            HostId = (string)reader["HostId"];
            JobStatus = (TimerJobStatus)(int)reader["JobStatus"];
            SuccessRuns = (int)reader["SuccessRuns"];
            FailedRuns = (int)reader["FailedRuns"];
            AvgTime = (int)reader["AvgTime"];
        }

        public string TimerJobId { get; set; }

        public string JobName { get; set; }

        public string PrepareJobScenario { get; set; }

        public string TimerScenario { get; set; }

        public int ExecutionPeriod { get; set; }

        public string HostId { get; set; }

        public TimerJobStatus JobStatus { get; set; }

        public int SuccessRuns { get; set; }

        public int FailedRuns { get; set; }

        public int AvgTime { get; set; }
    }
}
