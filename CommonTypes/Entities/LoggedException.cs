﻿
using System;
using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class LoggedException
    {
        public LoggedException() { }

        public LoggedException(SqlDataReader reader)
        {
            ExceptionId = (string)reader["ExceptionId"];
            EntityId = (string)reader["EntityId"];
            EntityType = (string)reader["EntityType"];
            ExceptionMessage = (string)reader["ExceptionMessage"];
            StackTrace = (string)reader["StackTrace"];
            ExceptionType = (string)reader["ExceptionType"];
            OccuredUtc = (DateTime)reader["OccuredUtc"];
        }

        public string ExceptionId { get; set; }

        public string EntityType { get; set; }

        public string EntityId { get; set; }

        public string ExceptionMessage { get; set; }

        public string StackTrace { get; set; }

        public string ExceptionType { get; set; }

        public DateTime OccuredUtc { get; set; }
    }
}
