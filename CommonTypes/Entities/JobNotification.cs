﻿using System.Data.SqlClient;

namespace CommonTypes.Entities
{
    public class JobNotification
    {
        public JobNotification() { }

        public JobNotification(SqlDataReader reader)
        {
            NotificationId = (string)reader["NotificationId"];
            NotificationName = (string)reader["NotificationName"];
            JobId = (string)reader["JobId"];
            HtmlTemplate = (string)reader["HtmlTemplate"];
            CriteriaCondition = new DataCondition(reader);
        }

        public string NotificationId { get; set; }

        public string NotificationName { get; set; }

        public DataCondition CriteriaCondition { get; set; }

        public string JobId { get; set; }

        public string HtmlTemplate { get; set; }

    }
}
