﻿using CommonTypes.Entities;

namespace CommonTypes.Messages
{
    public class StartJobMessage : BaseMessage
    {
        public TimerJob TimerJob { get; set; }
    }
}
