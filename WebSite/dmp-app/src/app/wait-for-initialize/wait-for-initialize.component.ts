import { Component, OnInit } from '@angular/core';
import { EnvironmentService } from '../services/environment.service';

@Component({
    selector: 'app-wait-for-initialize',
    templateUrl: './wait-for-initialize.component.html'
})
export class WaitForInitializeComponent implements OnInit {

    constructor(private environment: EnvironmentService) { }

    ngOnInit() {
        if (this.environment.apiUrl) {
            history.back();
        }

        this.environment.environmentInitialized.subscribe(() => {
            history.back();
        });
    }

}
