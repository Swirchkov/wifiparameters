import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ExecutionHost } from '../models/execution-host';

import 'rxjs/add/operator/map';

@Injectable()
export class ExecutionHostsService {

    constructor(private environment : EnvironmentService,
        private http: HttpClient) { }

    public getAvailableHosts(): Observable<ExecutionHost[]> {
        return this.http.get(this.environment.apiUrl + "api/executionhost")
            .map((arr : any ) => 
            arr.map(h => new ExecutionHost(h.HostId, h.HostName, h.HostUrl, h.AccountId)));
    }

    public addNewHost(host : ExecutionHost) {
        const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
        return this.http.post(this.environment.apiUrl + 'api/executionhost', JSON.stringify(host), 
            { headers : headers });
    }

    public deleteHost(hostId: string) {
        return this.http.delete(this.environment.apiUrl + "api/executionhost?entityId=" + hostId);
    }

}
