import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

    constructor(private environment : EnvironmentService,
        private http: HttpClient) { }

    public loginUser(email: string, password: string) {
        return this.http.post(this.environment.apiUrl + "api/user/login", 
            { email: email, password: password });
    }

    public updateUserEmail(userId: string, newEmail: string) {
        return this.http.put(this.environment.apiUrl + "api/user/updateemail", 
            { userId: userId, email: newEmail });
    }

    public updateUserPassword(userId: string, oldPassword: string, newPassword: string) {
        return this.http.put(this.environment.apiUrl + "api/user/updatepassword", 
            { userId: userId, oldPassword: oldPassword, newPassword: newPassword });
    }
}
