import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';
import { HttpClient } from '@angular/common/http';
import { HostParameters } from '../models/host-parameters';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class HostParamsService {

    constructor(private environment : EnvironmentService,
        private http: HttpClient) {}

    public getHostParameters(hostId: string) {
        return this.http.get(this.environment.apiUrl + "api/hostparams?hostId=" + hostId);
    }

    public addHostParameter(param : HostParameters) {
        const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
        return this.http.post(this.environment.apiUrl + "api/hostparams", JSON.stringify(param), 
            { headers: headers });
    }

    public deleteHostParam(paramId: string) {
        return this.http.delete(this.environment.apiUrl + "api/hostparams?entityId=" + paramId);
    }

}