import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JobNotification } from '../models/job-notification';

import 'rxjs/add/operator/map';

@Injectable()
export class JobNotificationService {

    constructor(private environment : EnvironmentService,
        private http: HttpClient) { }

    public getJobNotifications(jobId: string): Observable<JobNotification[]> {
        return this.http.get(this.environment.apiUrl + "api/jobnotification?jobId=" + jobId)
            .map((arr : any ) => arr.map(h => 
                new JobNotification(h.notificationId, h.notificationName, h.jobId, h.htmlTemplate, 
                     h.fieldName, h.fieldValue)));
    }

    public addJobNotification(notification : JobNotification) {
        const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
        return this.http.post(this.environment.apiUrl + 'api/jobnotification', 
            JSON.stringify(notification), { headers : headers });
    }

    public deleteJobNotification(notificationId: string) {
        return this.http.delete(
            this.environment.apiUrl + "api/jobnotification?entityId=" + notificationId);
    }

}