import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EnvironmentService {

    public apiUrl = null;

    constructor(private http : HttpClient) { }

    public get isReady() {
        return this.apiUrl != null;
    }

    public initialize() {
        this.http.get('./assets/env-urls.json').subscribe(urls => {
            this.apiUrl = urls['apiUrl'];
            this.environmentInitialized.emit();
        });
    }

    public environmentInitialized = new EventEmitter<void>();

}