import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ExtendedTimerJob } from '../models/extended-timer-job';

@Injectable()
export class TimerJobService {

    private headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});

    constructor(private environment : EnvironmentService,
        private http: HttpClient) {}

    public getTimerJobs(hostId: string) {
        return this.http.get(this.environment.apiUrl + "api/timerjob?hostId=" + hostId);
    }

    public addNewJob(job : ExtendedTimerJob) {
        return this.http.post(this.environment.apiUrl + 'api/timerjob', JSON.stringify(job), 
            { headers : this.headers });
    }

    public runJob(jobId: string) {
        return this.http.post(this.environment.apiUrl + "api/timerjob/runjob", JSON.stringify(jobId), 
            { headers: this.headers });
    }

    public stopJob(jobId: string) {
        return this.http.post(this.environment.apiUrl + "api/timerjob/stopjob", JSON.stringify(jobId), 
            { headers: this.headers });
    }

    public deleteJob(jobId: string) {
        return this.http.delete(this.environment.apiUrl + "api/timerjob?entityId=" + jobId);
    }

}