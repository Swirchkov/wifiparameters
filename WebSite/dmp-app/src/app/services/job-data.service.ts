import { EnvironmentService } from "./environment.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { JobData } from "../models/job-data";
import { Inject } from "@angular/core";
import { TableViewModel } from "../models/table-view-model";


export class JobDataService {
    
    constructor(@Inject(EnvironmentService) private environment : EnvironmentService,
        @Inject(HttpClient) private http: HttpClient) { }

    public getReceivedJobData(jobId: string): Observable<TableViewModel> {
        return this.http.get(this.environment.apiUrl + "api/jobdata?jobId=" + jobId)
            .map((resp : any ) => new TableViewModel(resp.columnHeaders, resp.rows));
    }
}
