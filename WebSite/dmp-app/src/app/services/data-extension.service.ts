import { Injectable } from '@angular/core';
import { EnvironmentService } from './environment.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JobDataExtension } from '../models/job-data-extension';

import 'rxjs/add/operator/map';

@Injectable()
export class DataExtensionService {

    constructor(private environment : EnvironmentService,
        private http: HttpClient) { }

    public getDataExtensions(jobId: string): Observable<JobDataExtension[]> {
        return this.http.get(this.environment.apiUrl + "api/dataextension?jobId=" + jobId)
            .map((arr : any ) => arr.map(h => 
                new JobDataExtension(h.dataExtensionId, h.timerJobId, h.dataColumnName, h.value,
                    h.dataCondition.fieldName, h.dataCondition.criteriaValue)));
    }

    public addNewDataExtension(extension : JobDataExtension) {
        const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});
        return this.http.post(this.environment.apiUrl + 'api/dataextension', 
            JSON.stringify(extension.toServerEntity()), { headers : headers });
    }

    public deleteExtension(dataExtensionId: string) {
        return this.http.delete(this.environment.apiUrl + "api/dataextension?entityId=" + dataExtensionId);
    }

}