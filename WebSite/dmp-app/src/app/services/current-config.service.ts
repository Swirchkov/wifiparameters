import { ExecutionHost } from "../models/execution-host";
import { TimerJob } from "../models/timer-job";
import { SessionStoredItem } from "../lib/session-stored-item";
import { User } from "../models/user";


export class CurrentConfigService {

    private _selectedHost: SessionStoredItem<ExecutionHost>;
    private _selectedJob: SessionStoredItem<TimerJob>;
    private _currentUser: SessionStoredItem<User>;
    private _currentAccountId: SessionStoredItem<string>;

    constructor() {
        this._selectedHost = new SessionStoredItem<ExecutionHost>('selectedHost');
        this._selectedJob = new SessionStoredItem<TimerJob>('selectedJob');
        this._currentUser = new SessionStoredItem<User>('currentUser');
        this._currentAccountId = new SessionStoredItem<string>('currentUserAccount');
    }

    public get accountId(): string {
        return this._currentAccountId.item;
    }

    public set accountId(val: string) {
        this._currentAccountId.item = val;
    }

    public get currentUser(): User {
        return this._currentUser.item;
    } 

    public set currentUser(value: User) {
        this._currentUser.item = value;
    }

    public get selectedHost(): ExecutionHost {
        return this._selectedHost.item;
    }

    public set selectedHost(value: ExecutionHost) {
        this._selectedHost.item = value;
    }

    public get selectedJob(): TimerJob {
        return this._selectedJob.item;
    }

    public set selectedJob(value : TimerJob) {
        this._selectedJob.item = value;
    }

}
