import { Component } from '@angular/core';
import { PasswordChangeModel } from '../models/password-change-model';
import { UserService } from '../services/user.service';
import { CurrentConfigService } from '../services/current-config.service';
import { User } from '../models/user';
import { EmailValidator } from '../validation/email.validator';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent {

    private profileModes = {
        None : 0,
        Email : 1,
        Password : 2
    };

    private currentMode: number = this.profileModes.None;

    private newEmail: string;

    private passwordChangeModel = new PasswordChangeModel('', '', '');

    private errorMessage: string;

    private hasError = false;

    private emailValidator = new EmailValidator();

    private previewText = 'The changes will be previewed here. Please start with selecting what you want to edit email or password.';

    constructor( protected userService: UserService,
        protected currentConfig: CurrentConfigService) { }

    switchToEmailEditing() {
        this.currentMode = this.profileModes.Email;
        this.reset();
    }

    switchToPasswordEditing() {
        this.currentMode = this.profileModes.Password;
        this.reset();
    }

    saveChanges() {
        if (this.currentMode === this.profileModes.Email) {

            if (!this.emailValidator.validateEmail(this.newEmail)) {
                this.errorMessage = 'Please enter an valid email address.';
                this.hasError = true;
                return;
            }

            this.userService.updateUserEmail(this.currentConfig.currentUser.userId, this.newEmail)
                .subscribe((resp) => {
                    
                    console.log(resp);
                    
                    this.currentMode = this.profileModes.None;
                    this.currentConfig.currentUser = 
                        new User(this.newEmail, this.currentConfig.currentUser.userId);
                    this.reset();
                    this.previewText = 'Email has been successfully changed.';

                }, (error) => {
                    console.log(error);
                });
        }
        else if (this.currentMode === this.profileModes.Password) {

            if (!this.validatePasswordChange()) {
                return;
            }

            this.userService.updateUserPassword(this.currentConfig.currentUser.userId, 
                this.passwordChangeModel.oldPassword, this.passwordChangeModel.newPassword)
                .subscribe((resp) => {
                    console.log(resp);
                    this.currentMode = this.profileModes.None;
                    this.reset();
                    this.previewText = 'Password has been successfully changed.';
                }, (resp) => {
                    this.errorMessage = resp.error.message;
                    this.hasError = true;
                });
        }

    }

    reset() {
        this.hasError = false;
        this.newEmail = '';
        this.passwordChangeModel = new PasswordChangeModel('', '', '');
    }

    validatePasswordChange(): boolean {
        this.hasError = false;
        console.log(this.passwordChangeModel);

        if (!this.passwordChangeModel.oldPassword) {
            this.hasError = true;
            this.errorMessage = "Please enter an old password";
        }
        else if (this.passwordChangeModel.newPassword !== this.passwordChangeModel.confirmPassword) {
            this.hasError = true;
            this.errorMessage = "New password doesn't match confimation.";
        }
        else if (this.passwordChangeModel.newPassword.length < 6) {
            this.hasError = true;
            this.errorMessage = "New password must be at least 6 characters";
        }

        return !this.hasError;
    }
}
