import { Component, OnInit } from '@angular/core';
import { EnvironmentService } from './services/environment.service';
import { CurrentConfigService } from './services/current-config.service';
import { MatDialog, MatIcon } from '@angular/material';
import { LoginComponent } from './login/login.component';
import { ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { ElementRef } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

    @ViewChild(MatMenuTrigger)
    private trigger: MatMenuTrigger;

    constructor(private environment: EnvironmentService,
        protected dialog: MatDialog,
        private currentConfig: CurrentConfigService,
        private element: ElementRef,
        protected router: Router) { }

    ngOnInit() {
        this.environment.initialize();
    }

    ngAfterViewInit() {
        var matIcon = (<HTMLElement>this.element.nativeElement).querySelector('mat-icon');

        if (this.trigger) {
            this.trigger.menuOpened.subscribe(() => {
                matIcon.textContent = 'arrow_drop_up';
            });

            this.trigger.menuClosed.subscribe(() => {
                matIcon.textContent = 'arrow_drop_down';
            });
        }
    }

    quit() {
        this.currentConfig.currentUser = null;
        this.router.navigate(['login']);
    }

}
