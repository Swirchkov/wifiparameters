import { Component } from '@angular/core';
import { CurrentConfigService } from '../services/current-config.service';

@Component({
    selector: 'app-job-detailed',
    templateUrl: './job-detailed.component.html',
    styleUrls: ['./job-detailed.component.css']
})
export class JobDetailedComponent {

    private managementModes = {
        JobExtensionData: 0,
        NotificationSubscription: 1
    };

    private currentMode: number = this.managementModes.JobExtensionData;

    constructor( protected currentConfig: CurrentConfigService ) { }

    switchToJobExtensions() {
        this.currentMode = this.managementModes.JobExtensionData;
    }

    switchToNotifications() {
        this.currentMode = this.managementModes.NotificationSubscription;
    }

}
