import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { JobData } from '../models/job-data';
import { BaseApiComponent } from '../base-api/base-api.component';
import { Router, ActivatedRoute } from '@angular/router';
import { EnvironmentService } from '../services/environment.service';
import { JobDataService } from '../services/job-data.service';
import { MatPaginator } from '@angular/material';
import { TableViewModel } from '../models/table-view-model';
import { CurrentConfigService } from '../services/current-config.service';

@Component({
    selector: 'app-jobs-data',
    templateUrl: './jobs-data.component.html',
    styleUrls: [
        '../shared-styles/table-component.css'
    ]
})
export class JobsDataComponent extends BaseApiComponent implements OnInit {

    private jobDataTable: TableViewModel;

    constructor(protected router: Router,
        protected environment: EnvironmentService,
        private route: ActivatedRoute, 
        private jobDataService: JobDataService,
        protected currentConfig: CurrentConfigService) {
        super(router, environment, currentConfig);
    }

    ngOnInit() {
        super.ngOnInit();

        if (this.environment.isReady) {
            this.route.params.subscribe((params) => {
                const jobId = params['timerJobId'];

                this.jobDataService.getReceivedJobData(jobId).subscribe((dataTable) => {
                    console.log(dataTable);
                    this.jobDataTable = dataTable
                });

            });
        }
    }

    back() {
        window.history.back();
    }

}
