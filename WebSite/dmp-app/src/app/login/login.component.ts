import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CurrentConfigService } from '../services/current-config.service';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent  {

    public email: string;

    public password: string;

    private isCredentialsValid = true;

    constructor(public currentConfig: CurrentConfigService,
        public userService: UserService) {}

    onLogin() {
        this.userService.loginUser(this.email, this.password).subscribe((resp: any) => {
            console.log(resp);
            if (resp) {
                this.currentConfig.currentUser = new User(this.email, resp.userId as string);
                this.currentConfig.accountId = resp.accountId;
                history.back();
            } else {
                this.isCredentialsValid = false;
            }
        });
    }
}