
export class SessionStoredItem<T> {

    private _item: T;

    constructor(private sessionStorageKey: string) {}

    public get item(): T {
        if (this._item) {
            return this._item;
        }

        if (sessionStorage.getItem(this.sessionStorageKey) != null) {
            this._item = <T>JSON.parse(sessionStorage.getItem(this.sessionStorageKey));
            return this._item;
        }

        return null;
    }

    public set item(value: T) {
        this._item = value;
        sessionStorage.setItem(this.sessionStorageKey, JSON.stringify(value));
    }

}
