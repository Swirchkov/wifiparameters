import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { JobDataExtension } from '../models/job-data-extension';
import { MatTableDataSource, MatTable, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { EnvironmentService } from '../services/environment.service';
import { TimerJobService } from '../services/timer-job.service';
import { BaseApiComponent } from '../base-api/base-api.component';
import { PopupDialogComponent } from '../dialogs/popup-dialog/popup-dialog.component';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { DataExtensionService } from '../services/data-extension.service';
import { CurrentConfigService } from '../services/current-config.service';
import { TimerJob } from '../models/timer-job';

@Component({
    selector: 'app-job-data-extension',
    templateUrl: './job-data-extension.component.html',
    styleUrls: [ '../shared-styles/table-component.css', '../shared-styles/inline-table-component.css' ]
})
export class JobDataExtensionComponent extends BaseApiComponent implements OnInit {
    
    private get displayedColumns() {
        if (this.currentConfig.currentUser) {
            return [ 'dataColumnName', 'value', 'dataCriteriaName', 'criteriaValue', 'actions' ];
        } else {
            return [ 'dataColumnName', 'value', 'dataCriteriaName', 'criteriaValue' ];
        }
    }

    private extDataSource = new MatTableDataSource<JobDataExtension>([]);

    @ViewChild(MatTable)
    private table: MatTable<JobDataExtension>;

    @Input()
    private job: TimerJob;

    constructor(protected router: Router,
        protected environment: EnvironmentService,
        protected route: ActivatedRoute, 
        protected dataExtensionsService: DataExtensionService,
        protected dialog: MatDialog,
        protected currentConfig: CurrentConfigService) {
            super(router, environment, currentConfig);
        }

    ngOnInit() {
        super.ngOnInit();

        if (this.environment.isReady) {
            this.dataExtensionsService.getDataExtensions(this.job.timerJobId).subscribe(
                (dataExtensions : any) => {
                this.extDataSource.data = dataExtensions;
            });
        }
    }

    back() {
        window.history.back();
    }

    openAddDialog() {
        const dialogRef = this.dialog.open(PopupDialogComponent, {
            data: {
                headerMessage: 'Add new data extension',
                submitMessage: 'Add',
                fields: [
                    {
                        name: 'Column name',
                        value: ''
                    },
                    {
                        name: 'Value',
                        value: ''
                    },
                    {
                        name: 'Criteria name',
                        value: ''
                    },
                    {
                        name: 'Filter value',
                        value: ''
                    }
                ]
            }
        });

        dialogRef.afterClosed().subscribe((res) => {

            if (res) {
                var dataExtension = new JobDataExtension('', this.job.timerJobId, res[0].value, 
                    res[1].value, res[2].value, res[3].value);

                this.dataExtensionsService.addNewDataExtension(dataExtension).subscribe((res: any) => {
                    dataExtension.dataExtensionId = res;
                    this.addOrCreateDataExtension(dataExtension);
                    if (this.table) {
                        this.table.renderRows();
                    }
                });
            }
        });
    }

    deleteDataExtension(dataExtension: JobDataExtension) {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                bodyMessage: 'Are you sure you want to delete available job data extension?',
                headerMessage: 'Remove data extension',
                color: 'warn',
                buttonMessage: 'Delete'
            }
        });

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.dataExtensionsService.deleteExtension(dataExtension.dataExtensionId).subscribe((res) => {
                    this.extDataSource.data = 
                        this.extDataSource.data.filter(j => j.dataExtensionId != dataExtension.dataExtensionId);
                    this.table.renderRows();
                });
            }
        });
    }

    private addOrCreateDataExtension(jobExtension: JobDataExtension) {
        if (this.extDataSource.data) {
            this.extDataSource.data.push(jobExtension);
        } else {
            this.extDataSource.data = [ jobExtension ];
        }
    }

}
