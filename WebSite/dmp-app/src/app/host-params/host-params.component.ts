import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';

import { HostParameters } from '../models/host-parameters';
import { BaseApiComponent } from '../base-api/base-api.component';
import { EnvironmentService } from '../services/environment.service';
import { HostParamsService } from '../services/host-params.service';
import { MatDialog, MatTable } from '@angular/material';
import { PopupDialogComponent } from '../dialogs/popup-dialog/popup-dialog.component';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { CurrentConfigService } from '../services/current-config.service';

@Component({
    selector: 'app-host-params',
    templateUrl: './host-params.component.html',
    styleUrls: ['../shared-styles/table-component.css']
})
export class HostParamsComponent extends BaseApiComponent implements OnInit {

    private get displayedColumns() : string[] {
        if (this.currentConfig.currentUser) {
            return [ 'paramName', 'paramValue', 'actions' ];
        }
        else {
            return [ 'paramName', 'paramValue' ];
        }
    }

    private paramsDataSource = new MatTableDataSource<HostParameters>();

    private hostId: string;

    @ViewChild(MatTable)
    private table: MatTable<HostParameters>;

    constructor(protected router: Router,
        protected environment: EnvironmentService,
        private route: ActivatedRoute,
        private hostParamsService: HostParamsService,
        private dialog: MatDialog,
        protected currentConfig: CurrentConfigService) {
            super(router, environment, currentConfig);
        }

    ngOnInit() {
        super.ngOnInit();

        if (this.environment.apiUrl) {
            this.route.params.subscribe(params => {
                let hostId = params['hostId'];
                this.hostId = hostId;

                this.hostParamsService.getHostParameters(hostId).subscribe((params: any) => {
                    this.paramsDataSource.data = params.map(p => 
                        new HostParameters(p.ParameterId, p.ParameterName, p.ParameterValue, p.HostId));
                });
            });
        }
    }

    back() {
        window.history.back();
    }

    openAddDialog() {
        const dialogRef = this.dialog.open(PopupDialogComponent, {
            data: {
                headerMessage: 'Add new host parameter',
                submitMessage: 'Add',
                fields: [
                    {
                        name: 'Parameter name',
                        value: ''
                    },
                    {
                        name: 'Parameter value',
                        value: ''
                    }
                ]
            }
        });

        dialogRef.afterClosed().subscribe((res) => {

            if (res) {
                var createdparam = new HostParameters('', res[0].value, res[1].value, this.hostId);

                this.hostParamsService.addHostParameter(createdparam).subscribe( (res : any) => {
                    createdparam.parameterId = res;
                    this.paramsDataSource.data.push(createdparam);
                    this.table.renderRows();
                });
            }
        });
    }

    deleteHostParam(hostParam: HostParameters) {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                headerMessage: 'Remove host input parameter',
                bodyMessage: 'Are you sure that you want to delete input parameter to all host related jobs?',
                color: 'warn',
                buttonMessage: 'Delete'
            }
        });

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.hostParamsService.deleteHostParam(hostParam.parameterId).subscribe((res) => {
                    this.paramsDataSource.data = 
                        this.paramsDataSource.data.filter(h => h.parameterId != hostParam.parameterId);
                    this.table.renderRows();
                });
            }
        });
    }
}
