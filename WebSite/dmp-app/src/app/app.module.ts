import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule, MatInputModule, MatPaginatorModule, MatMenuModule, MatExpansionModule } from '@angular/material';

import { AppComponent } from './app.component';
import { ExecutionHostComponent } from './execution-host/execution-host.component';
import { JobsComponent } from './jobs/jobs.component';
import { HostParamsComponent } from './host-params/host-params.component';
import { JobsDataComponent } from './jobs-data/jobs-data.component';
import { EnvironmentService } from './services/environment.service';
import { ExecutionHostsService } from './services/execution-hosts.service';
import { WaitForInitializeComponent } from './wait-for-initialize/wait-for-initialize.component';
import { BaseApiComponent } from './base-api/base-api.component';
import { HostParamsService } from './services/host-params.service';
import { TimerJobService } from './services/timer-job.service';
import { PopupDialogComponent } from './dialogs/popup-dialog/popup-dialog.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { CurrentConfigService } from './services/current-config.service';
import { JobDataService } from './services/job-data.service';
import { JobDataExtensionComponent } from './job-data-extension/job-data-extension.component';
import { DataExtensionService } from './services/data-extension.service';
import { ReadOnlyTableComponent } from './read-only-table/read-only-table.component';
import { InformationPopupComponent } from './dialogs/information-popup/information-popup.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './services/user.service';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { JobDetailedComponent } from './job-detailed/job-detailed.component';
import { JobNotificationComponent } from './job-notification/job-notification.component';
import { JobNotificationService } from './services/job-notification.service';
import { JobNotificationAddComponent } from './job-notification-add/job-notification-add.component';
import { PreviewHtmlDialogComponent } from './dialogs/preview-html-dialog/preview-html-dialog.component';

const appRoutes: Routes = [
    { path: 'executionhosts', component: ExecutionHostComponent },
    { path: '', component: ExecutionHostComponent },
    { path: 'wait', component: WaitForInitializeComponent },
    { path: 'jobs/:hostId', component: JobsComponent },
    { path: 'params/:hostId', component: HostParamsComponent },
    { path: 'jobdata/:timerJobId', component: JobsDataComponent },
    { path: 'dataextension/:jobId', component: JobDataExtensionComponent },
    { path: 'login', component: LoginComponent },
    { path: 'editprofile', component: EditProfileComponent },
    { path: 'jobdetailed/:jobId', component: JobDetailedComponent },
    { path: 'jobnotificationadd', component: JobNotificationAddComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        ExecutionHostComponent,
        JobsComponent,
        HostParamsComponent,
        JobsDataComponent,
        WaitForInitializeComponent,
        BaseApiComponent,
        PopupDialogComponent,
        ConfirmDialogComponent,
        JobDataExtensionComponent,
        ReadOnlyTableComponent,
        InformationPopupComponent,
        LoginComponent,
        EditProfileComponent,
        JobDetailedComponent,
        JobNotificationComponent,
        JobNotificationAddComponent,
        PreviewHtmlDialogComponent
    ],
    imports: [
        BrowserModule,
        MatTableModule,
        MatButtonModule,
        MatIconModule,
        MatDialogModule,
        MatInputModule,
        HttpClientModule,
        BrowserAnimationsModule,
        FormsModule,
        MatPaginatorModule,
        MatMenuModule,
        MatExpansionModule,
        RouterModule.forRoot(
            appRoutes
            // { enableTracing: true } 
        )
    ],
    entryComponents: [
        PopupDialogComponent,
        ConfirmDialogComponent,
        InformationPopupComponent,
        LoginComponent,
        PreviewHtmlDialogComponent
    ],
    providers: [
        EnvironmentService,
        ExecutionHostsService,
        HostParamsService, 
        TimerJobService,
        CurrentConfigService,
        JobDataService,
        DataExtensionService,
        UserService,
        JobNotificationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
