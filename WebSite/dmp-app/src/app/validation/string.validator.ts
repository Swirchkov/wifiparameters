
export class StringValidator {

    public static isNullOrEmpty(s: string) {
        return !s || s === '';
    }

}
