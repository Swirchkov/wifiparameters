
export class EmailValidator {

    public validateEmail(email: string) : boolean {
        const regex = /\w+\@\w+\.\w+/gm;
        return regex.test(email);
    }

}
