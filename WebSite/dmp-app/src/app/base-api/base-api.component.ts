import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EnvironmentService } from '../services/environment.service';
import { CurrentConfigService } from '../services/current-config.service';

@Component({
    selector: 'app-base-api',
    templateUrl: './base-api.component.html'
})
export class BaseApiComponent implements OnInit {

    constructor(protected router : Router,
        protected environment: EnvironmentService,
        protected currentConfig: CurrentConfigService) { }

    ngOnInit() {
        if (!this.environment.apiUrl) {
            this.router.navigate(['wait']);
        }
        if (!this.currentConfig.currentUser) {
            this.router.navigate(['login']);
        }
    }

}
