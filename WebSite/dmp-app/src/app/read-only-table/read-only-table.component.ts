import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { TableViewModel } from '../models/table-view-model';
import { MatTableDataSource, MatPaginator } from '@angular/material';

@Component({
    selector: 'app-read-only-table',
    templateUrl: './read-only-table.component.html',
    styleUrls: ['../shared-styles/table-component.css']
})
export class ReadOnlyTableComponent implements OnInit, AfterViewInit {

    @Input('table')
    public tableData: TableViewModel;

    @ViewChild(MatPaginator)
    public paginator: MatPaginator;

    public tableDataSource = new MatTableDataSource<string[]>()

    constructor() { }

    ngOnInit() {
        this.tableDataSource.data = this.tableData.rows;
        this.tableDataSource.paginator = this.paginator;
        console.log(this);
    }

    ngAfterViewInit() {
        console.log(this.paginator);
        this.tableDataSource.paginator = this.paginator;
    }

}
