
export class HostParameters {

    constructor( public parameterId: string,
        public parameterName: string,
        public parameterValue: string,
        public hostId: string) {}

}
