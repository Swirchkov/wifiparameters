
export class JobDataExtension {

    constructor(public dataExtensionId: string,
        public timerJobId: string,
        public dataColumnName: string,
        public value: string,
        public dataCriteriaName: string,
        public criteriaValue: string) {}

    public toServerEntity() {
        return {
            dataExtensionId: this.dataExtensionId,
            timerJobId: this.timerJobId,
            dataColumnName: this.dataColumnName,
            value: this.value,
            dataCondition: {
                fieldName: this.dataCriteriaName,
                criteriaValue: this.criteriaValue
            }
        }
    }
}
