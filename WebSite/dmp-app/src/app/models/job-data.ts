
export class JobData {

    constructor(public jobDataId: string,
        public jobId: string,
        public retrieveName: string,
        public retrievedData: string ) {} 

}
