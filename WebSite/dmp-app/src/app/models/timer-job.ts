
export class TimerJob {

    constructor(public timerJobId: string,
        public jobName: string,
        public executionPeriod: number, 
        public hostId: string,
        public jobStatus: string) { }

}
