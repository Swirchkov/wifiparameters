
export class JobNotification {

    constructor(public notificationId: string,
        public notificationName: string,
        public jobId: string,
        public htmlTemplate: string,
        public fieldName: string,
        public fieldValue: string) {}

}
