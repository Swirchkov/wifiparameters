import { TimerJob } from "./timer-job";


export class ExtendedTimerJob extends TimerJob {

    constructor(public timerJobId: string,
        public jobName: string,
        public executionPeriod: number,
        public hostId: string,
        public jobStatus: string,
        public prepareJobScenario: string,
        public timerScenario: string) { 
            super(timerJobId, jobName, executionPeriod, hostId, jobStatus);
        }

}
