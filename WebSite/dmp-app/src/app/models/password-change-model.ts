
export class PasswordChangeModel {

    constructor (public oldPassword: string, public newPassword: string,
        public confirmPassword: string) {}

}
