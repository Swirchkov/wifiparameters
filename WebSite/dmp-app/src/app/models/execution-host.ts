
export class ExecutionHost {

    constructor(public hostId: string,
        public hostName: string,
        public hostUrl: string, 
        public accountId: string) { }

}
