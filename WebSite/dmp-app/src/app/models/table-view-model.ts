
export class TableViewModel {

    constructor(public columnHeaders: string[], 
        public rows: Array<Array<string>>) {}

}
