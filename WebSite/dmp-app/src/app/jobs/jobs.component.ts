import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { TimerJob } from '../models/timer-job';
import { BaseApiComponent } from '../base-api/base-api.component';
import { Router, ActivatedRoute } from '@angular/router';
import { EnvironmentService } from '../services/environment.service';
import { TimerJobService } from '../services/timer-job.service';
import { MatDialog, MatTable } from '@angular/material';
import { PopupDialogComponent } from '../dialogs/popup-dialog/popup-dialog.component';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { ExtendedTimerJob } from '../models/extended-timer-job';
import { CurrentConfigService } from '../services/current-config.service';
import { InformationPopupComponent } from '../dialogs/information-popup/information-popup.component';

@Component({
    selector: 'app-jobs',
    templateUrl: './jobs.component.html',
    styleUrls: ['../shared-styles/table-component.css']
})
export class JobsComponent extends BaseApiComponent implements OnInit {

    private get displayedColumns() { 
        if (this.currentConfig.currentUser) {
            return  [ 'jobName', 'jobPeriod', 'jobStatus', 'jobManagement', 'viewData', 
                'viewData1', 'actions' ];
        }
        else {
            return  [ 'jobName', 'jobPeriod', 'jobStatus', 'viewData', 'viewData1' ];
        }
    }

    private jobStatus = {
        Stopped: 'Stopped',
        Running: 'Running',
        StopRequested: 'StopRequested'
    };

    private jobDataSource = new MatTableDataSource<TimerJob>([]);

    @ViewChild(MatTable)
    private table: MatTable<TimerJob>;

    private hostId: string;

    constructor(protected router: Router,
        protected environment: EnvironmentService,
        protected route: ActivatedRoute, 
        protected jobsService: TimerJobService,
        protected dialog: MatDialog, 
        protected currentConfig: CurrentConfigService) {
            super(router, environment, currentConfig);
        }

    ngOnInit() {
        super.ngOnInit();

        if (this.environment.isReady) {
            this.route.params.subscribe((params) => {
                const hostId = params['hostId'];

                this.hostId = hostId;

                this.jobsService.getTimerJobs(hostId).subscribe((jobs : any) => {
                    console.log(jobs);
                    this.jobDataSource.data = jobs.map(j => 
                        new TimerJob(j.timerJobId, j.jobName, j.executionPeriod, j.hostId, j.jobStatus));
                });

            });
        }
    }

    back() {
        window.history.back();
    }

    openAddDialog() {
        const dialogRef = this.dialog.open(PopupDialogComponent, {
            data: {
                headerMessage: 'Add new job',
                submitMessage: 'Add',
                fields: [
                    {
                        name: 'Job name',
                        value: ''
                    },
                    {
                        name: 'Job execution period',
                        value: ''
                    },
                    {
                        name: 'Prepare scenario',
                        value: ''
                    },
                    {
                        name: 'Timer scenario',
                        value: ''
                    }
                ]
            }
        });

        dialogRef.afterClosed().subscribe((res) => {

            if (res) {
                var createdjob = new ExtendedTimerJob('', res[0].value, +res[1].value, this.hostId, 
                    "Stopped", res[2].value, res[3].value);

                this.jobsService.addNewJob(createdjob).subscribe( (res : any) => {
                    createdjob.timerJobId = res;
                    this.addOrCreateJob(createdjob);
                    if (this.table) {
                        this.table.renderRows();
                    }
                });
            }
        });
    }

    deleteJob(job: TimerJob) {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                bodyMessage: 'Are you sure you want to delete available job with all related data?',
                headerMessage: 'Remove timer job',
                color: 'warn',
                buttonMessage: 'Delete'
            }
        });

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.jobsService.deleteJob(job.timerJobId).subscribe((res) => {
                    this.jobDataSource.data = 
                        this.jobDataSource.data.filter(j => j.timerJobId != job.timerJobId);
                    this.table.renderRows();
                });
            }
        });
    }

    runJob(job: TimerJob) {

        this.currentConfig.selectedJob = job;
        this.jobsService.runJob(job.timerJobId).subscribe((res) => {
            const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                data: {
                    bodyMessage: 'Do you want to view retrieved data?',
                    headerMessage: 'Job executed successfully',
                    color: 'primary',
                    buttonMessage: 'View data'
                }
            });
    
            dialogRef.afterClosed().subscribe((res) => {
                if (res) {
                    this.router.navigate(['/jobdata', job.timerJobId]);
                }
            });
        });
    }

    goToJobData(job: TimerJob) {
        this.currentConfig.selectedJob = job;
        this.router.navigate(['/jobdata', job.timerJobId]);
    }

    goToJobDetails(job: TimerJob) {
        this.currentConfig.selectedJob = job;
        this.router.navigate(['/jobdetailed', job.timerJobId]);
    }

    stopJob(job: TimerJob) {
        console.log(job.timerJobId);
        this.jobsService.stopJob(job.timerJobId).subscribe((res) => {
            const dialogRef = this.dialog.open(InformationPopupComponent, {
                data: {
                    bodyMessage: 'The job will be stopped during next timer iteration.',
                    headerMessage: 'Information'
                }
            });
            job.jobStatus = 'StopRequested';
        });
    }

    goToExtensionData(job : TimerJob) {
        this.currentConfig.selectedJob = job;
        this.router.navigate(['dataextension', job.timerJobId]);
    }

    private addOrCreateJob(job: ExtendedTimerJob) {
        if (this.jobDataSource.data) {
            this.jobDataSource.data.push(job);
        } else {
            this.jobDataSource.data = [ job ];
        }
    }
}
