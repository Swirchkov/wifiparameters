import { Component, OnInit, ViewChild } from '@angular/core';
import { StringValidator } from '../validation/string.validator';
import { JobNotificationService } from '../services/job-notification.service';
import { JobNotification } from '../models/job-notification';
import { CurrentConfigService } from '../services/current-config.service';

enum ShowRequiredMessage {
    None = 0,
    HtmlTemplate = 1,
    FieldName = 2,
    FieldValue = 3
}

@Component({
    selector: 'app-job-notification-add',
    templateUrl: './job-notification-add.component.html',
    styleUrls: ['./job-notification-add.component.scss', '../shared-styles/table-component.css' ]
})
export class JobNotificationAddComponent implements OnInit {

    public notificationName: string = '';

    public htmlTemplate: string = '<p> Preview </p>';

    public fieldName: string = '';
    public fieldValue: string = '';

    public showTemplateRequiredMessage = ShowRequiredMessage.None;
    public showRequiredMessage = ShowRequiredMessage;

    constructor(protected readonly notificationService: JobNotificationService,
        protected readonly currentConfig: CurrentConfigService) { }

    ngOnInit() {


    }

    saveNotification() {
        if (StringValidator.isNullOrEmpty(this.htmlTemplate)) {
            this.showTemplateRequiredMessage = ShowRequiredMessage.HtmlTemplate;
            return;
        }

        if (StringValidator.isNullOrEmpty(this.fieldName)) {
            this.showTemplateRequiredMessage = ShowRequiredMessage.FieldName;
            return;
        }

        if (StringValidator.isNullOrEmpty(this.fieldValue)) {
            this.showTemplateRequiredMessage = ShowRequiredMessage.FieldValue;
            return;
        }

        this.showTemplateRequiredMessage = ShowRequiredMessage.None;

        var notification = new JobNotification(null, this.notificationName, 
            this.currentConfig.selectedJob.timerJobId, this.htmlTemplate, this.fieldName, this.fieldValue);

        this.notificationService.addJobNotification(notification).subscribe((res) => {
            if (res) {
                window.history.back();
            }
        });
    }

    back() {
        window.history.back();
    }

}
