import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { BaseApiComponent } from '../base-api/base-api.component';
import { JobNotification } from '../models/job-notification';
import { MatTableDataSource, MatTable, MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { EnvironmentService } from '../services/environment.service';
import { JobNotificationService } from '../services/job-notification.service';
import { CurrentConfigService } from '../services/current-config.service';
import { TimerJob } from '../models/timer-job';
import { PopupDialogComponent } from '../dialogs/popup-dialog/popup-dialog.component';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { PreviewHtmlDialogComponent } from '../dialogs/preview-html-dialog/preview-html-dialog.component';

@Component({
  selector: 'app-job-notification',
  templateUrl: './job-notification.component.html',
  styleUrls: ['../shared-styles/table-component.css', '../shared-styles/inline-table-component.css' ]
})
export class JobNotificationComponent extends BaseApiComponent implements OnInit {
    
    private get displayedColumns() {
        if (this.currentConfig.currentUser) {
            return [ 'htmlTemplate', 'dataCriteriaName', 'criteriaValue', 'actions' ];
        } else {
            return [ 'htmlTemplate', 'dataCriteriaName', 'criteriaValue' ];
        }
    }

    private tableDataSource = new MatTableDataSource<JobNotification>([]);

    @ViewChild(MatTable)
    private table: MatTable<JobNotification>;

    @Input()
    private job: TimerJob;

    constructor(protected router: Router,
        protected environment: EnvironmentService,
        protected route: ActivatedRoute, 
        protected notificationsService: JobNotificationService,
        protected dialog: MatDialog,
        protected currentConfig: CurrentConfigService) {
            super(router, environment, currentConfig);
        }

    ngOnInit() {
        super.ngOnInit();

        this.notificationsService.getJobNotifications(this.job.timerJobId).subscribe(
        (notificationSubscriptions : any) => {
            this.tableDataSource.data = notificationSubscriptions;
        });
    }

    back() {
        window.history.back();
    }

    addNotification() {
        this.router.navigate([ '/jobnotificationadd' ]);
    }

    deleteNotification(notification: JobNotification) {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                bodyMessage: 'Are you sure you want to delete available notification?',
                headerMessage: 'Remove notification',
                color: 'warn',
                buttonMessage: 'Delete'
            }
        });

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.notificationsService.deleteJobNotification(notification.notificationId)
                .subscribe((res) => {
                    this.tableDataSource.data = this.tableDataSource.data.
                        filter(j => j.notificationId != notification.notificationId);
                    this.table.renderRows();
                });
            }
        });
    }

    previewNotification(notification: JobNotification) {
        this.dialog.open(PreviewHtmlDialogComponent, {
            data: {
                dialogHeader: 'Preview notification ' + notification.notificationName,
                template: notification.htmlTemplate
            }
        });
    }

}

