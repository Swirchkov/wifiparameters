import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';

import { ExecutionHost } from '../models/execution-host';
import { ExecutionHostsService } from '../services/execution-hosts.service';
import { BaseApiComponent } from '../base-api/base-api.component';
import { EnvironmentService } from '../services/environment.service';
import { MatDialog, MatTable } from '@angular/material';
import { PopupDialogComponent } from '../dialogs/popup-dialog/popup-dialog.component';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { CurrentConfigService } from '../services/current-config.service';

@Component({
    selector: 'app-execution-host',
    templateUrl: './execution-host.component.html',
    styleUrls: ['../shared-styles/table-component.css']
})
export class ExecutionHostComponent extends BaseApiComponent implements OnInit {

    private displayedColumns = ['hostName', 'hostUrl', 'actions'];

    private hostsDataSource = new MatTableDataSource<ExecutionHost>([]);

    @ViewChild(MatTable)
    private table: MatTable<ExecutionHost>;

    constructor(
        protected router: Router,
        protected environment: EnvironmentService,
        protected hostService: ExecutionHostsService,
        protected dialog: MatDialog,
        protected currentConfig: CurrentConfigService) {
        super(router, environment, currentConfig);
    }

    ngOnInit() {
        super.ngOnInit();
        
        if (this.environment.apiUrl) {
            this.hostService.getAvailableHosts().subscribe(hosts => {
                this.hostsDataSource.data = hosts;
            });
        }
    }

    openAddDialog() {
        const dialogRef = this.dialog.open(PopupDialogComponent, {
            data: {
                headerMessage: 'Add new host',
                submitMessage: 'Add',
                fields: [
                    {
                        name: 'Host name',
                        value: ''
                    },
                    {
                        name: 'Host url',
                        value: ''
                    }
                ]
            }
        });

        dialogRef.afterClosed().subscribe((res) => {

            if (res) {
                var createdhost = new ExecutionHost('', res[0].value, res[1].value, 
                    this.currentConfig.accountId);

                this.hostService.addNewHost(createdhost).subscribe( (res : any) => {
                    createdhost.hostId = res;
                    this.hostsDataSource.data.push(createdhost);
                    this.table.renderRows();
                });
            }
        });
    }

    deleteHost(host: ExecutionHost) {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                bodyMessage: 'Are you sure you want to delete available host with all related data?',
                headerMessage: 'Remove available host',
                color: 'warn',
                buttonMessage: 'Delete'
            }
        });

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.hostService.deleteHost(host.hostId).subscribe((res) => {
                    this.hostsDataSource.data = 
                        this.hostsDataSource.data.filter(h => h.hostId != host.hostId);
                    this.table.renderRows();
                });
            }
        });
    }

    goToJobs(host: ExecutionHost) {
        this.currentConfig.selectedHost = host;
        this.router.navigate(['jobs', host.hostId]);
    }

    goToParams(host: ExecutionHost) {
        this.currentConfig.selectedHost = host;
        this.router.navigate(['params', host.hostId]);
    }

}
