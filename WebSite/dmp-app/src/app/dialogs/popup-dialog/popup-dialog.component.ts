import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'app-popup-dialog',
    templateUrl: './popup-dialog.component.html',
    styleUrls: ['./popup-dialog.component.css']
})
export class PopupDialogComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<PopupDialogComponent>, 
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit() {
        console.log('dialog popup');
        console.log(this.data);
    }

    onCancelClick() {
        this.dialogRef.close();
    }

}
