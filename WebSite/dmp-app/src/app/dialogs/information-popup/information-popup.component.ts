import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-information-popup',
    templateUrl: './information-popup.component.html',
    styleUrls: ['./information-popup.component.css']
})
export class InformationPopupComponent {

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

}
