import { Component } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Inject } from "@angular/core";


@Component({
    selector: 'app-preview-html-dialog',
    templateUrl: './preview-html-dialog.html',
    styleUrls: [ './preview-html-dialog.css' ]
})
export class PreviewHtmlDialogComponent {

    constructor(public dialogRef: MatDialogRef<PreviewHtmlDialogComponent>, 
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    close() {
        this.dialogRef.close();
    }

}
