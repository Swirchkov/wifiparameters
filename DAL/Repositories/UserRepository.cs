﻿using CommonTypes.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiParameters.DAL.Repositories
{
    class UserRepository : Repository<User>, IRepository<User>
    {
        protected override User CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new User(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(User entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@UserId", entity.UserId),
                new SqlParameter("@AccountId", entity.AccountId),
                new SqlParameter("@Email", entity.Email),
                new SqlParameter("@Password", entity.Password)
            };
        }

        protected override void PrepareEntity(User entity)
        {
            if (string.IsNullOrWhiteSpace(entity.UserId))
            {
                entity.UserId = Guid.NewGuid().ToString();
            }
        }
    }
}
