﻿using CommonTypes.Entities;
using System;
using System.Data.SqlClient;

namespace WifiParameters.DAL.Repositories
{
    class DataExtensionRepository : Repository<JobDataExtension>, IRepository<JobDataExtension>
    {
        protected override JobDataExtension CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new JobDataExtension(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(JobDataExtension entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@DataExtensionId", entity.DataExtensionId),
                new SqlParameter("@TimerJobId", entity.TimerJobId),
                new SqlParameter("@DataColumnName", entity.DataColumnName),
                new SqlParameter("@Value", entity.Value),
                new SqlParameter("@DataConditionId", entity.DataCondition.DataConditionId),
                new SqlParameter("@DataCriteriaName", entity.DataCondition.FieldName),
                new SqlParameter("@CriteriaValue", entity.DataCondition.CriteriaValue)
            };
        }

        protected override void PrepareEntity(JobDataExtension entity)
        {
            if (string.IsNullOrWhiteSpace(entity.DataExtensionId))
            {
                entity.DataExtensionId = Guid.NewGuid().ToString();
            }
            if (string.IsNullOrWhiteSpace(entity.DataCondition.DataConditionId))
            {
                entity.DataCondition.DataConditionId = Guid.NewGuid().ToString();
            }
        }

        protected override string DbSourceName => "vJobDataExtension";
    }
}
