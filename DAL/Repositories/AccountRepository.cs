﻿using CommonTypes.Entities;
using System;
using System.Data.SqlClient;

namespace WifiParameters.DAL.Repositories
{
    class AccountRepository : Repository<Account>, IRepository<Account>
    {
        protected override Account CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new Account(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(Account entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@AccountId", entity.AccountId),
                new SqlParameter("@AccountName", entity.AccountName)
            };
        }

        protected override void PrepareEntity(Account entity)
        {
            if (string.IsNullOrWhiteSpace(entity.AccountId))
            {
                entity.AccountId = Guid.NewGuid().ToString();
            }
        }
    }
}
