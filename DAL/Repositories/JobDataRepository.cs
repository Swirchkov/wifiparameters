﻿using CommonTypes;
using CommonTypes.Entities;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;
using WifiParameters.DAL.RepositoryPrototypes;
using CommonTypes.ViewModels;

namespace WifiParameters.DAL.Repositories
{
    class JobDataRepository : Repository<JobData>, IRepository<JobData>, IJobDataRepository
    {
        protected override JobData CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new JobData(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(JobData entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@JobDataId", entity.JobDataId),
                new SqlParameter("@JobId", entity.JobId),
                new SqlParameter("@RetrieveName", entity.RetrieveName),
                new SqlParameter("@RetrievedData", entity.RetrievedData),
                new SqlParameter("@RowIndex", entity.RowIndex)
            };
        }

        protected override void PrepareEntity(JobData entity)
        {
            if (string.IsNullOrWhiteSpace(entity.JobDataId))
            {
                entity.JobDataId = Guid.NewGuid().ToString();
            }
        }

        public void ClearAllJobData(string jobId)
        {
            ExecuteCommand(command =>
            {
                command.Parameters.Add(new SqlParameter("@JobId", jobId));
                command.CommandText = DBQueries.JobData.DeleteDataByJob;
                command.ExecuteNonQuery();
            });
        }

        public void DeleteBy(IEnumerable<FilterCriteria> filters)
        {
            ExecuteCommand(command =>
            {
                command.CommandText = string.Format(DBQueries.JobData.DeleteDataByFilter, FilterCriteria.BuildWhereClause(filters));
                command.ExecuteNonQuery();
            });
        }
    }
}
