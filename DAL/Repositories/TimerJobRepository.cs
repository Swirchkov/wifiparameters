﻿using CommonTypes.Entities;
using System.Data.SqlClient;
using System;

namespace WifiParameters.DAL.Repositories
{
    class TimerJobRepository : Repository<TimerJob>
    {
        protected override TimerJob CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new TimerJob(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(TimerJob entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@TimerJobId", entity.TimerJobId),
                new SqlParameter("@JobName", entity.JobName),
                new SqlParameter("@PrepareJobScenario", entity.PrepareJobScenario),
                new SqlParameter("@TimerScenario", entity.TimerScenario),
                new SqlParameter("@ExecutionPeriod", entity.ExecutionPeriod),
                new SqlParameter("@HostId", entity.HostId),
                new SqlParameter("@JobStatus", entity.JobStatus),
                new SqlParameter("@SuccessRuns", entity.SuccessRuns),
                new SqlParameter("@FailedRuns", entity.FailedRuns),
                new SqlParameter("@AvgTime", entity.AvgTime)
            };
        }

        protected override void PrepareEntity(TimerJob entity)
        {
            if (string.IsNullOrWhiteSpace(entity.TimerJobId))
            {
                entity.TimerJobId = Guid.NewGuid().ToString();
            }
        }

    }
}
