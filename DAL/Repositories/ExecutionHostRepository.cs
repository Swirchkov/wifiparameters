﻿using CommonTypes;
using CommonTypes.Entities;
using System.Collections.Generic;
using System.Data.SqlClient;
using WifiParameters.DAL.RepositoryPrototypes;
using System.Linq;
using System;

namespace WifiParameters.DAL.Repositories
{
    class ExecutionHostRepository : Repository<JobExecutionHost>, IRepository<JobExecutionHost>, IExecutionHostRepository
    {

        protected override JobExecutionHost CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new JobExecutionHost(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(JobExecutionHost entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@HostId", entity.HostId),
                new SqlParameter("@HostName", entity.HostName),
                new SqlParameter("@HostUrl", entity.HostUrl),
                new SqlParameter("@AccountId", entity.AccountId)
            };
        }

        protected override void PrepareEntity(JobExecutionHost entity)
        {
            if (string.IsNullOrWhiteSpace(entity.HostId))
            {
                entity.HostId = Guid.NewGuid().ToString();
            }
        }

        public IEnumerable<JobExecutionHost> GetAllHosts()
        {
            return ExecuteCommand(command => 
            {
                command.CommandText = DBQueries.JobExecutionHost.GetAllHosts;

                return GetEntitiesFromReader(command.ExecuteReader());
            });
        }
    }
}
