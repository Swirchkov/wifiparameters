﻿using CommonTypes.Entities;
using System;
using System.Data.SqlClient;

namespace WifiParameters.DAL.Repositories
{
    class DataConditionRepository : Repository<DataCondition>, IRepository<DataCondition>
    {
        protected override DataCondition CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new DataCondition(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(DataCondition entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@DataConditionId", entity.DataConditionId),
                new SqlParameter("@FieldName", entity.FieldName),
                new SqlParameter("@CriteriaValue", entity.CriteriaValue)
            };
        }

        protected override void PrepareEntity(DataCondition entity)
        {
            if (string.IsNullOrWhiteSpace(entity.DataConditionId))
            {
                entity.DataConditionId = Guid.NewGuid().ToString();
            }
        }
    }
}
