﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonTypes.Entities;

namespace WifiParameters.DAL.Repositories
{
    class JobNotificationRepository : Repository<JobNotification>
    {
        protected override JobNotification CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new JobNotification(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(JobNotification entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@NotificationId", entity.NotificationId),
                new SqlParameter("@DataConditionId", entity.CriteriaCondition.DataConditionId),
                new SqlParameter("@NotificationName", entity.NotificationName),
                new SqlParameter("@JobId", entity.JobId),
                new SqlParameter("@HtmlTemplate", entity.HtmlTemplate),
                new SqlParameter("@CriteriaFieldName", entity.CriteriaCondition.FieldName),
                new SqlParameter("@CriteriaValue", entity.CriteriaCondition.CriteriaValue)
            };
        }

        protected override void PrepareEntity(JobNotification entity)
        {
            if (string.IsNullOrWhiteSpace(entity.NotificationId))
            {
                entity.NotificationId = Guid.NewGuid().ToString();
            }

            if (string.IsNullOrWhiteSpace(entity.CriteriaCondition.DataConditionId))
            {
                entity.CriteriaCondition.DataConditionId = Guid.NewGuid().ToString();
            }
        }

        protected override string DbSourceName => "vJobNotification";
    }
}
