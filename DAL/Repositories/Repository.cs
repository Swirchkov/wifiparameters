﻿using CommonTypes;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WifiParameters.DAL.InternalStructures;

namespace WifiParameters.DAL.Repositories
{
    public abstract class Repository<TCommonEntity> : IRepository<TCommonEntity> where TCommonEntity: class
    {
        #region abstract memebers

        protected abstract void PrepareEntity(TCommonEntity entity);

        protected abstract SqlParameter[] GetSqlParametersForEntity(TCommonEntity entity);

        protected abstract TCommonEntity CreateFromSqlDataReader(SqlDataReader reader);

        protected virtual string DbSourceName { get; } = TypeToDBNamesMapper<TCommonEntity>.TableName;

        #endregion

        public void Add(TCommonEntity entity)
        {
            PrepareEntity(entity);

            ExecuteCommand(command =>
            {
                command.Parameters.AddRange(GetSqlParametersForEntity(entity));
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = TypeToDBNamesMapper<TCommonEntity>.AddProcedureName;

                command.ExecuteNonQuery();
            });
        }

        public void Delete(string id)
        {
            ExecuteCommand(command =>
            {
                command.Parameters.Add(new SqlParameter("@Id", id));
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = TypeToDBNamesMapper<TCommonEntity>.DeleteProcedureName;
                command.ExecuteNonQuery();
            });
        }

        public IEnumerable<TCommonEntity> FindBy(IEnumerable<FilterCriteria> filters)
        {
            return ExecuteCommand(command =>
            {
                command.CommandText = string.Format(DBQueries.FilteredEntities, DbSourceName, FilterCriteria.BuildWhereClause(filters));

                return GetEntitiesFromReader(command.ExecuteReader());
            });
        }

        public TCommonEntity Get(string id)
        {
            return ExecuteCommand(command =>
            {
                command.Parameters.Add(new SqlParameter("@Id", id));
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = TypeToDBNamesMapper<TCommonEntity>.GetByIdProcedureName;

                var reader = command.ExecuteReader();

                if (reader.Read())
                {
                    return CreateFromSqlDataReader(reader);
                }

                return null;
            });
        }

        public void Update(TCommonEntity entity)
        {
            ExecuteCommand(command =>
            {
                command.Parameters.AddRange(GetSqlParametersForEntity(entity));

                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = TypeToDBNamesMapper<TCommonEntity>.UpdateProcedureName;

                command.ExecuteNonQuery();
            });
        }

        protected void ExecuteCommand(Action<SqlCommand> commandProcessor)
        {
            using (SqlConnection connection = new SqlConnection(DbManagementData.ConnectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                commandProcessor(command);

                connection.Close();
            }
        }

        protected T ExecuteCommand<T>(Func<SqlCommand, T> commandProcessor)
        {
            using (SqlConnection connection = new SqlConnection(DbManagementData.ConnectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                var commandData = commandProcessor(command);

                connection.Close();
                return commandData;
            }
        }

        protected IEnumerable<TCommonEntity> GetEntitiesFromReader(SqlDataReader reader)
        {
            var entities = new List<TCommonEntity>();

            while (reader.Read())
            {
                entities.Add(CreateFromSqlDataReader(reader));
            }

            return entities;
        }
    }
}
