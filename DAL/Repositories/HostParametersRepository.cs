﻿using CommonTypes.Entities;
using System;
using System.Data.SqlClient;

namespace WifiParameters.DAL.Repositories
{
    class HostParametersRepository : Repository<HostParameters>, IRepository<HostParameters>
    {
        protected override HostParameters CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new HostParameters(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(HostParameters entity)
        {
            return new SqlParameter[] 
            {
                new SqlParameter("@ParameterId", entity.ParameterId),
                new SqlParameter("@ParameterName", entity.ParameterName),
                new SqlParameter("@ParameterValue", entity.ParameterValue),
                new SqlParameter("@HostId", entity.HostId)
            };
        }

        protected override void PrepareEntity(HostParameters entity)
        {
            if (string.IsNullOrWhiteSpace(entity.ParameterId))
            {
                entity.ParameterId = Guid.NewGuid().ToString();
            }
        }
    }
}
