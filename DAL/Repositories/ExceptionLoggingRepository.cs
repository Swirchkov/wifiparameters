﻿using CommonTypes.Entities;
using System;
using System.Data.SqlClient;

namespace WifiParameters.DAL.Repositories
{
    class ExceptionLoggingRepository : Repository<LoggedException>, IRepository<LoggedException>
    {
        protected override LoggedException CreateFromSqlDataReader(SqlDataReader reader)
        {
            return new LoggedException(reader);
        }

        protected override SqlParameter[] GetSqlParametersForEntity(LoggedException entity)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@ExceptionId", entity.ExceptionId),
                new SqlParameter("@EntityType", entity.EntityType),
                new SqlParameter("@EntityId", entity.EntityId),
                new SqlParameter("@ExceptionMessage", entity.ExceptionMessage),
                new SqlParameter("@ExceptionType", entity.ExceptionType),
                new SqlParameter("@StackTrace", entity.StackTrace),
                new SqlParameter("@OccuredUtc", entity.OccuredUtc)
            };
        }

        protected override void PrepareEntity(LoggedException entity)
        {
            if (string.IsNullOrWhiteSpace(entity.ExceptionId))
            {
                entity.ExceptionId = Guid.NewGuid().ToString();
            }
        }
    }
}
