﻿
using CommonTypes;
using System.Collections.Generic;

namespace WifiParameters.DAL
{
    public interface IRepository<T> where T: class
    {
        T Get(string id);

        IEnumerable<T> FindBy(IEnumerable<FilterCriteria> filters);

        void Add(T entity);

        void Update(T entity);

        void Delete(string id);
    }
}
