﻿using CommonTypes;
using CommonTypes.Entities;
using CommonTypes.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiParameters.DAL.RepositoryPrototypes
{
    public interface IJobDataRepository : IRepository<JobData>
    {
        void ClearAllJobData(string jobId);

        void DeleteBy(IEnumerable<FilterCriteria> filters);
    }
}
