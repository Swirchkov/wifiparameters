﻿using CommonTypes.Entities;
using System.Collections.Generic;

namespace WifiParameters.DAL.RepositoryPrototypes
{
    public interface IExecutionHostRepository: IRepository<JobExecutionHost>
    {
        IEnumerable<JobExecutionHost> GetAllHosts();
    }
}
