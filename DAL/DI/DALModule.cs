﻿using Autofac;
using CommonTypes.Entities;
using WifiParameters.DAL.Repositories;
using WifiParameters.DAL.RepositoryPrototypes;

namespace WifiParameters.DAL.DI
{
    public class DALModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ExecutionHostRepository>().As<IRepository<JobExecutionHost>>();
            builder.RegisterType<ExecutionHostRepository>().As<IExecutionHostRepository>();

            builder.RegisterType<HostParametersRepository>().As<IRepository<HostParameters>>();

            builder.RegisterType<TimerJobRepository>().As<IRepository<TimerJob>>();

            builder.RegisterType<JobDataRepository>().As<IRepository<JobData>>();
            builder.RegisterType<JobDataRepository>().As<IJobDataRepository>();

            builder.RegisterType<DataConditionRepository>().As<IRepository<DataCondition>>();
            builder.RegisterType<JobNotificationRepository>().As<IRepository<JobNotification>>();

            builder.RegisterType<DataExtensionRepository>().As<IRepository<JobDataExtension>>();
            builder.RegisterType<ExceptionLoggingRepository>().As<IRepository<LoggedException>>();

            builder.RegisterType<AccountRepository>().As<IRepository<Account>>();
            builder.RegisterType<UserRepository>().As<IRepository<User>>();

            base.Load(builder);
        }
    }
}
