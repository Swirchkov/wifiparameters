﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiParameters.DAL.InternalStructures
{
    class TypeToDBNamesMapper<TCommonEntity>
    {
        private static string BasicNamePart
        {
            get
            {
                return typeof(TCommonEntity).Name;
            }
        }

        public static string AddProcedureName
        {
            get
            {
                return BasicNamePart + "Add";
            }
        }

        public static string UpdateProcedureName
        {
            get
            {
                return BasicNamePart + "Update";
            }
        }

        public static string DeleteProcedureName
        {
            get
            {
                return BasicNamePart + "Delete";
            }
        }

        public static string GetByIdProcedureName
        {
            get
            {
                return BasicNamePart + "GetById";
            }
        }

        public static string TableName
        {
            get
            {
                return BasicNamePart;
            }
        }
    }
}
