﻿
namespace WifiParameters.DAL
{
    static class DBQueries
    {
        public static readonly string FilteredEntities = @"SELECT * FROM [{0}] WHERE {1};";

        public static string DeleteDataByFilter = @"DELETE FROM {0} WHERE {1}";

        public static class JobExecutionHost
        {
            public static string GetAllHosts = @"SELECT * FROM JobExecutionHost;";

            public static string FilteredHosts = @"SELECT * FROM JobExecutionHost WHERE {0};";

            public static string GetHostById = @" SELECT * FROM JobExecutionHost 
                WHERE HostId = @HostId;";

            public static string AddHost = @"INSERT INTO JobExecutionHost 
                (HostId, HostName, HostUrl) 
                VALUES ( @HostId, @HostName, @HostUrl);";

            public static string DeleteHost = @" DELETE FROM JobExecutionHost 
                WHERE HostId = @HostId;";
        }

        public static class HostParameters
        {
            public static string FilteredParamss = @"SELECT * FROM HostParameters WHERE {0};";

            public static string GetHostParameterById = @"SELECT * FROM HostParameters
                WHERE ParameterId = @ParameterId;";

            public static string AddHostParameter = @"INSERT INTO HostParameters   
                ( ParameterId, ParameterName, ParameterValue, HostId) 
                VALUES ( @ParameterId, @ParameterName, @ParameterValue, @HostId);";

            public static string DeleteHostParameter = @"DELETE FROM HostParameters
                WHERE ParameterId = @ParameterId;";
        }

        public static class TimerJob
        {
            public static string FilteredJobs = @"SELECT * FROM TimerJob WHERE {0};";

            public static string GetTimerJobById = @"SELECT * FROM TimerJob WHERE TimerJobId = @TimerJobId";

            public static string AddTimerJob = @"INSERT INTO TimerJob (TimerJobId, JobName, PrepareJobScenario, TimerScenario, ExecutionPeriod, HostId, JobStatus, SuccessRuns, FailedRuns, AvgTime) VALUES (@TimerJobId, @JobName, @PrepareJobScenario, @TimerScenario, @ExecutionPeriod, @HostId, @JobStatus, @SuccessRuns, @FailedRuns, @AvgTime);";

            public static string UpdateTimerJob = @"UPDATE TimerJob SET JobName=@JobName, PrepareJobScenario = @PrepareJobScenario, TimerScenario = @TimerScenario, ExecutionPeriod = @ExecutionPeriod, HostId = @HostId, JobStatus = @JobStatus, SuccessRuns = @SuccessRuns, FailedRuns = @FailedRuns, AvgTime = @AvgTime WHERE TimerJobId = @TimerJobId";

            public static string DeleteTimerJob = @"DELETE FROM TimerJob WHERE TimerJobId = @TimerJobId";
        }

        public static class JobData
        {
            public static string Filtered = @"SELECT * FROM JobData WHERE {0};";

            public static string GetJobDataId = @"SELECT * FROM JobData WHERE JobDataId = @JobDataId;";

            public static string DeleteDataByJob = @"DELETE FROM JobData WHERE JobId=@JobId";

            public static string DeleteDataByFilter = @"DELETE FROM JobData WHERE {0}";

            public static string AddJobData = @"INSERT INTO JobData (JobDataId, RetrieveName, RetrievedData, JobId, RowIndex) VALUES ( @JobDataId, @RetrieveName, @RetrievedData, @JobId, @RowIndex);";

            public static string DeleteJobData = @"DELETE FROM JobData WHERE JobDataId = @JobDataId;";
        }

        public static class JobDataExtensions
        {
            public static string Filtered = @"SELECT * FROM JobDataExtension WHERE {0};";

            public static string GetDataExtensionById = @"SELECT * FROM JobDataExtension WHERE DataExtensionId = @DataExtensionId;";

            public static string AddDataExtension = @"INSERT INTO JobDataExtension (DataExtensionId, TimerJobId, DataColumnName, Value, DataCriteriaName, CriteriaValue) 
                VALUES ( @DataExtensionId, @TimerJobId, @DataColumnName, @Value, @DataCriteriaName, @CriteriaValue);";

            public static string DeleteJobDataExtension = @"DELETE FROM JobDataExtension 
                WHERE DataExtensionId = @DataExtensionId;";
        }

        public static class LoggedException
        {
            public static string FilteredExceptions = @"SELECT * FROM LoggedExceptions WHERE {0};";

            public static string GetExceptionById = @"SELECT * FROM LoggedExceptions WHERE ExceptionId = @ExceptionId;";

            public static string AddLoggedException = @"INSERT INTO LoggedExceptions ( ExceptionId, EntityType, EntityId, OccuredUtc, ExceptionMessage, StackTrace, ExceptionType) VALUES ( @ExceptionId, @EntityType, @EntityId, @OccuredUtc, @ExceptionMessage, @StackTrace, @ExceptionType);";

            public static string DeleteLoggedException = @"DELETE FROM LoggedExceptions WHERE ExceptionId = @ExceptionId;";
        }

    }
}
